(deffunction ask-question (?question $?allowed-values)
   (printout t ?question)
   (bind ?answer (read))
   (if (lexemep ?answer) 
       then (bind ?answer (lowcase ?answer)))
   (while (not (member ?answer ?allowed-values)) do
      (printout t ?question)
      (bind ?answer (read))
      (if (lexemep ?answer) 
          then (bind ?answer (lowcase ?answer))))
   ?answer)

(deffunction yes-or-no-p (?question)
   (bind ?response (ask-question ?question yes no y n))
   (if (or (eq ?response yes) (eq ?response y))
       then TRUE 
       else FALSE))
       
(defrule determinesexuality ""
   (not (sexuality ?))
   (not (sex ?))
   =>
   (if (yes-or-no-p "Do you like men (yes/no)? ") 
       then
       (if (yes-or-no-p "Are you gay (yes/no)? ")
           then (assert (sexuality (printout t "Gay?" crlf)))
           else (assert (sexuality (printout t "No Idea?" crlf))))
       else 
       (assert (sexuality (printout t "Straight?" crlf)))))
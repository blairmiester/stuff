(deftemplate person
	(slot name)
	(slot age)
	(slot interest)
	(slot town)
	(slot sex)
	)


(deffacts desperate
	(person (name "Steve Smith")
		(age 38)
		(interest meercats)
		(town musselburgh)
		(sex male))
	(person (name "Dave Cran")
		(age 27)
		(interest swinging)
		(town edinburgh)
		(sex male))
	(person (name "Uri Nus")
		(age 42)
		(interest reading)
		(town musselburgh)
		(sex male))
	(person (name "Clit Oris")
		(age 21)
		(interest gardening)
		(town tranent)
		(sex female))
	(person (name "Brenda Hera")
		(age 25)
		(interest fashion)
		(town edinburgh)
		(sex female))
	(person (name "Yuna Sanj")
		(age 39)
		(interest meercats)
		(town musselburgh)
		(sex female))
)

(deffacts interest_types
	(interest_type gardening outdoor)
	(interest_type swinging outdoor)
	(interest_type meercats indoor)
	(interest_type fashion indoor)
	)


(deffacts towns
	(near musselburgh edinburgh)
	(near edinburgh tranent))

(defrule town_adjacency "Reduces facts required"
	(near ?x ?y)
	=>
	(assert (near ?y ?x)))

(defrule location1 "Locations match if both people in the same town"
	(possible_match ?name1 ?name2)
	(person (name ?name1) (town ?t1))
	(person (name ?name2) (town ?t1))
	=>
	(assert (locations_match ?name1 ?name2)))


(defrule location2 "Locations match if the two towns are nearby"
	(possible_match ?name1 ?name2)
	(person (name ?name1) (town ?t1))
	(person (name ?name2) (town ?t2))
	(near ?t1 ?t2)
	=>
	(assert (locations_match ?name1 ?name2)))


(defrule ages "This agency thinks the woman should be younger"
	(possible_match ?her_name ?his_name)
	(person (name ?her_name) (age ?her_age))
	(person (name ?his_name) (age ?his_age))
	(test (< ?her_age ?his_age))
	=>
	(assert (ages_match ?her_name ?his_name)))
	
 
(defrule hobbies
	(possible_match ?name1 ?name2)
	(person (name ?name1) (interest ?interest1))
	(person (name ?name2) (interest ?interest2))
	(interest_type ?interest1 ?type)
	(interest_type ?interest2 ?type)
	=>
	(assert (hobbies_match ?name1 ?name2)))


(defrule possible_match 
	(person (name ?name1) (sex female))
	(person (name ?name2) (sex male))
	=>
	(assert (possible_match ?name1 ?name2)))


(defrule match
	(possible_match ?name1 ?name2)
	(locations_match ?name1 ?name2)
	(ages_match ?name1 ?name2)
	(hobbies_match ?name1 ?name2)
	=>
	(assert (match ?name1 ?name2)))
       

(deffunction newPerson (?r)
	(printout t "What is your name?" crlf)
	(bind ?name (readline))
	(printout t "What is your age?" crlf)
	(bind ?age (readline))
	(printout t "What is your interest? (meercats, swinging, reading, gardening, fashion)" crlf)
	(bind ?interest (readline))
	(printout t "What is your town?" crlf)
	(bind ?town (readline))
	(printout t "What is your sex (male OR female)?" crlf)
	(bind ?sex (readline))
	(assert (person (name ?name)(age ?age)(interest ?interest)(town ?town)(sex ?sex))))
	
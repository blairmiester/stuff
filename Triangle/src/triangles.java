import java.util.*;
import java.io.*;

public class triangles 
{

public static void main(String[] args) throws FileNotFoundException, IOException
{

File results = new File("results.txt");
BufferedWriter output = null;
output = new BufferedWriter(new FileWriter(results));

int nTriangles;
double sidea;
double sideb;
double sidec;
int i = 0;


Scanner scanner = new Scanner(new File("test.txt"));
int [] tri = new int [100];
int t = 0;
while(scanner.hasNextInt())
{
   tri[t++] = scanner.nextInt();
}

nTriangles = tri[i++];

System.out.println("There are " + nTriangles + " triangles in the file");
System.out.println();

while(nTriangles>0)
{
	sidea = tri[i++];
	sideb = tri[i++];
	sidec = tri[i++];
	
	output.write(String.valueOf(sidea));
	output.write(" ");
	output.write(String.valueOf(sideb));
	output.write(" ");
	output.write(String.valueOf(sidec));
	output.write(" ");
	

System.out.println("Sides" + " " + sidea + " " + sideb + " " + sidec);


if (!(((sidea + sideb) >= (sidec)) && ((sidea + sideb) >= (sideb)) && ((sideb + sidec) >= (sidea))))
{
	System.out.println("not a triangle");
	System.out.println();
	output.write("NOT A TRIANGLE");
	output.newLine();
	nTriangles --;
	
}


else if ((((sidea * sidea) + (sideb * sideb)) == (sidec * sidec))||(((sidea * sidea) + (sidec * sidec)) == (sideb * sideb))||((sidec * sidec) + (sideb * sideb)) == (sidea * sidea))
{
	System.out.println("right triangle");
	output.write("right");
	output.write(" ");
	findAngle(sidea, sideb, sidec, output);
	output.newLine();
	nTriangles --;
}

//right triangle

else if ((sidea == sideb) && (sidea == sidec) && (sideb == sidec))
{
	System.out.println("equilateral triangle");
	output.write("equilateral");
	output.write(" ");
	findAngle(sidea, sideb, sidec, output);
	output.newLine();
	nTriangles --;
}
//equilateral triangle

else if (((sidea == sideb) || (sideb == sidec) || (sidea == sidec)) && !((sidea == sideb) && (sideb == sidec) && (sidea == sidec)))
{
	System.out.println("isosceles triangle");
	output.write("isoceles");
	output.write(" ");
	findAngle(sidea, sideb, sidec, output);
	output.newLine();
	nTriangles --;
}

//isosceles triangle

else if (!((sidea == sideb) && (sidea == sidec) && (sideb == sidec)))
{
	System.out.println("scalene triangle");
	output.write("scalene");
	output.write(" ");
	findAngle(sidea, sideb, sidec, output);
	output.newLine();
	nTriangles --;
}

//scalene triangle
}
output.close();
}


public static void findAngle(double sidea, double sideb, double sidec, BufferedWriter output) throws IOException
{
	
double max = 0;

double angleA = Math.toDegrees(Math.acos((sidea * sidea + sideb * sideb - sidec * sidec) / (2 * sidea * sideb)));
double angleB = Math.toDegrees(Math.acos((sidea * sidea + sidec * sidec - sideb * sideb) / (2 * sidea * sidec)));
double angleC = Math.toDegrees(Math.acos((sideb * sideb + sidec * sidec - sidea * sidea) / (2 * sideb * sidec)));


if (angleA >= angleB && angleA >= angleC) 
{
	max = angleA;
}
else if (angleB >= angleA && angleB >= angleC)
{
	max = angleB;
}
else if (angleC >= angleA && angleC >= angleB)
{
	max = angleC;
}

if (max == 90)
{
	System.out.println("Right Angled");
	output.write("Right");
	System.out.println();
}
else if(max < 90)
{
	System.out.println("Acute");
	output.write("Acute");
	System.out.println();
}
else if(max > 90)
{
	System.out.println("Obtuse");
	output.write("Obtuse");
	System.out.println();
}

//System.out.println(max);
}


}
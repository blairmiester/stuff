//
//  body.cpp
//  Scorpion
//
//  Created by Blair Murray and Josh Stevenson on 17/10/2012.
//  Copyright (c) 2012 Blair Murray. All rights reserved.
//

#include <stdlib.h>
//#include <GL/glut.h>
#include <glut/glut.h>
#include <math.h>
#include <stdio.h>
#include "3DCurve.h"
//#include "cube.h"
#include "body.h"
#include "b.h"

void drawBody()
{
    
    glPushMatrix();
    glRotatef(90, 0, 0, 1);
    glScalef(1.5, 1.5, 1.5);

    drawLetterB();
    
    
    
    glTranslatef(0, 0, 0.1);
    glRotatef(-20, 0, 1, 0);
    
    drawLetterB();
    
    
    glTranslatef(0, 0, 0.1);
    glRotatef(-20, 0, 1, 0);
    
    drawLetterB();
    
    
    glTranslatef(0, 0, 0.1);
    glRotatef(-20, 0, 1, 0);
    
    drawLetterB();
    
    
    glTranslatef(0, 0, 0.1);
    glRotatef(-20, 0, 1, 0);
    
    drawLetterB();
    
    
    glTranslatef(0, 0, 0.1);
    glRotatef(-10, 0, 1, 0);
    
    drawLetterB();
    
    
    glPopMatrix();

    
    
    
    
    
    glPushMatrix();
    glRotatef(90, 0, 0, 1);
    glScalef(1.5, 1.5, 1.5);
    
    drawLetterB();
    
    
    
    glTranslatef(0, 0, -0.1);
    glRotatef(20, 0, 1, 0);
    
    drawLetterB();
    
    
    glTranslatef(0, 0, -0.1);
    glRotatef(20, 0, 1, 0);
    
    drawLetterB();
    
    
    glTranslatef(0, 0, -0.1);
    glRotatef(20, 0, 1, 0);
    
    drawLetterB();
    
    
    glTranslatef(0, 0, -0.1);
    glRotatef(20, 0, 1, 0);
    
    drawLetterB();
    
    
    glTranslatef(0, 0, -0.1);
    glRotatef(10, 0, 1, 0);
    
    drawLetterB();
    
    
    glPopMatrix();
}

#include <stdlib.h>
//#include <GL/glut.h>
#include <glut/glut.h>
#include <math.h>
#include <stdio.h>
#include "3DCurve.h"
#include "cube.h"
#include "j.h"

static double theta_stop1 = 90;
//======================================================
// DRAW_Letter_J
//======================================================
void drawLetterJ()
{
	
	    glPushMatrix();
        glTranslatef(0.0,0.0,0.0); //Place rectangle 1 here.
        glScalef(4.0,0.5,1.0);
        cube();
        glPopMatrix();
        
		glPushMatrix();
        glTranslatef(0.0,-3,0.0); //Place rectangle 1 here.
        glScalef(0.5,6.0,1.0);
        cube();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(-1.5,-7,0.0); //Place rectangle 1 here.
        glScalef(1.0,0.5,1.0);
        cube();
        glPopMatrix();
    
    glPushMatrix();
    
    glTranslatef(-1, -6, 0);
    glRotatef(270, 0, 0, 1);
    glScalef(0.5, 0.5, 1);
    
    draw3Dcurve  (1.0,          //depth
				  1.5,          //inner radius
				  2.5,          //outer radius
				  0.0,          //start angle
				  theta_stop1,  //stop angle
				  5.0);         //anular increments
    
    
    
    glPopMatrix();
}
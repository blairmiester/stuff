// Example_8_3.cpp : Mechanical Arm Example
//
// Author  : Blair Murray, Josh Stevenson
// Date    : 29/10/2012
// Version : 1.8
//
// This program shows how to use a hierarchical model to save a great deal of problems when
// attempting to rotate a model at different points (in this case, at each arm joint)
//
// Program behaviour:
//
//
// Callback commentary sent to normal command window.

//#include "stdafx.h"
#include <stdlib.h>
//#include <GL/glut.h>
#include <glut/glut.h>
#include <math.h>
#include <stdio.h>
#include "3DCurve.h"
#include "b.h"
#include "l.h"
#include "m.h"
#include "g.h"
#include "j.h"
#include "s.h"
#include "body.h"
#include "claw.h"
#include "drawScorpion.h"
#include "pincer.h"
#include "tail.h"

//======================================================
// GLOBAL VARIABLES 
//======================================================

#define TIME_INTERVAL 75

//for model, floor and grid display
int current_model=1;
int dont_display_floor=0; 
int x_y_display=0, y_z_display=0, x_z_display=0;
int iterations;

//for view control
static float G_theta[3]; // View X,Y,Z
static float G_zoom=0.3;

bool MousePressed; // Used in Mouse Drag to change the Viewpoint
float pitch0, yaw0;
int mouseX0, mouseY0;

//for articulation of SCORPION
static float PINCER_ONE_ROTATE=10, PINCER_TWO_ROTATE=-10, CLAWROTATE=0, ARMROTATE=120, MOVELEG=0,TAIL_ROTATE=-70;





//CURVE

static double theta_stop1 = 90;
float pitch = 0.0f;
float yaw = 0.0f;
bool rotating=false;

////////////////////////////////

//======================================================
// PROTOTYPES 
//======================================================

void drawScene(float hand_rot, float palm_angle, float f1_angle, float f2_angle, float f3_angle);
void viewControl();
void drawFloor();
//void drawBody();
void action(unsigned char key);
void menuCallback (int id);
 
 
//======================================================
// DRAW AXES and GRIDS
//======================================================
void drawAxesAndGridLines(void)
{
	float offset; int gd;
	glBegin(GL_LINES);
		glColor3f(1, 0, 0);
		glVertex3f(-20, 0, 0);					
		glVertex3f(+20, 0, 0);					
		glVertex3f( 0 ,-20, 0);				    	
		glVertex3f(	0, +20, 0);
		glVertex3f( 0, 0,-20);				    	
		glVertex3f(	0, 0, +20);

	glEnd();
	
	glLineStipple(1, 0xAAAA); //line style = fine dots
	glEnable(GL_LINE_STIPPLE);

	glBegin(GL_LINES);
		
		if (x_y_display) {glColor3f(0.0,0.7,0.7);
		for (offset=-10.0;offset<10.1;offset++){
			//draw lines in x-y plane
			glVertex3f(-10.0, offset, 0.0);					// Top Left
			glVertex3f(+10.0, offset, 0.0);					// Top Right
			glVertex3f( offset,-10, 0.0);					// Bottom Right
			glVertex3f(	offset,+10.0, 0.0);					// Bottom Left
		}}

		if (y_z_display) {glColor3f(0.7,0.0,0.7);
		for (offset=-10.0;offset<10.1;offset++){
			//draw lines in y-z plane
			glVertex3f( 0, offset, -10);					
			glVertex3f(	0, offset, 10.0);
			glVertex3f( 0, -10, offset);					
			glVertex3f(	0, 10, offset);
		}}

		if (x_z_display) {glColor3f(0.7,0.7,0.0);
		for (offset=-10.0;offset<10.1;offset++){
			//draw lines in x-z plane
			glVertex3f( offset, 0, -10);					
			glVertex3f(	offset, 0, 10.0);
			glVertex3f( -10, 0, offset);					
			glVertex3f(	10, 0, offset);
		}}

	glEnd();
	glDisable(GL_LINE_STIPPLE);

}

//======================================================
// DRAW_FLOOR
//======================================================
void drawFloor()
{
	if (dont_display_floor) return; 
	//Just draw a square for the floor
	glPushMatrix(); 
		glTranslatef(0, -0.05, 0); //draw slightly below y=0 so we can see grid
		glBegin(GL_POLYGON);
			glColor3f(.75,.75,.75);
			glVertex3f(-10,0,10);
			glVertex3f(-10,0,-10);
			glVertex3f(10,0,-10);
			glVertex3f(10,0,10);
		glEnd();
	glPopMatrix();
}

//======================================================
// DRAW_SCENE 
//======================================================

void drawScene()
{
	drawAxesAndGridLines();

	glPushMatrix();	
    
	switch(current_model)
	{
		case 1:
                glScalef(0.8, 0.8, 0.8);
            
                drawScorpion(PINCER_ONE_ROTATE, PINCER_TWO_ROTATE, CLAWROTATE, ARMROTATE, MOVELEG, TAIL_ROTATE);
		break;

		default:
			printf("Unknown model\n");
	}

	glPopMatrix();
}


//======================================================
// LOAD MENU ROUTINE 
//======================================================
void loadMenu()
{
	// This is the menu shown when you right click on the program display.
	glutCreateMenu(menuCallback);
	glutAddMenuEntry("Scorpion[m]", 1);
	glutAddMenuEntry("Animation[e]", 'e');
	glutAddMenuEntry("Zoom out(z)", 'z');
	glutAddMenuEntry("Zoom in(Z)", 'Z');
	glutAddMenuEntry("Toggle x-y grid", 'x');
	glutAddMenuEntry("Toggle y-z grid", 'y');
	glutAddMenuEntry("Toggle x-z grid", 'X');

    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

//======================================================
// MENU CALLBACK ROUTINE 
//======================================================
void menuCallback (int id)
{
  switch (id)
    {
        case 1:
        current_model=id;  
	    glutPostRedisplay();
        break;
        default: action (id);
    }
}

//======================================================
// TIMER FUNCTIONS
//======================================================
void update(int value)
{
    MOVELEG+=5;
    if (MOVELEG==30)
    {
        MOVELEG=-20;
    }
    
    
    
    PINCER_ONE_ROTATE+=10, PINCER_TWO_ROTATE-=10;
    
    if (PINCER_ONE_ROTATE==20 && PINCER_TWO_ROTATE==-20)
    {
        PINCER_ONE_ROTATE=0;
        PINCER_TWO_ROTATE=0;
    }
    
    
    
    
    CLAWROTATE-=5;
    
    if (CLAWROTATE == -50)
    {
        CLAWROTATE = 0;
    }
    
    
        
    ARMROTATE-=5;
    
    if (ARMROTATE==90)
    {
        ARMROTATE=150;
    }
    
    

    //Use a timer to animate scorpion
    if (iterations>1)
    {
        glutTimerFunc(TIME_INTERVAL, update,iterations);
        --iterations;
    }
    glutPostRedisplay();
}


//======================================================
// KEYBOARD CALLBACK ROUTINES
//======================================================
void keyboardCallBack(unsigned char key, int x, int y)
{
	printf("Keyboard call back: key=%c, x=%d, y=%d\n", key, x, y);
	action (key);
}

void action(unsigned char key)
{

	switch(key)
	{

    case 'h':   //move legs
            
            MOVELEG+=10;
            if (MOVELEG==30)
            {
                MOVELEG=-20;
            }
            glutPostRedisplay();
            
            break;
            
            
	case 'g':   //rotate pincer
            
            PINCER_ONE_ROTATE+=10, PINCER_TWO_ROTATE-=10;
            
            if (PINCER_ONE_ROTATE==30 && PINCER_TWO_ROTATE==-30)
            {
                PINCER_ONE_ROTATE=0;
                PINCER_TWO_ROTATE=0;
            }
            
            glutPostRedisplay();
            
            
            
            break;
            
    case 'f':   //roate claw
            
                CLAWROTATE-=10;
            
            if (CLAWROTATE == -50)
            {
                CLAWROTATE = 0;
            }
            glutPostRedisplay();
            
            break;
            
            
            
    case 'd':   //rotate arm
            
            ARMROTATE-=10;
            
            if (ARMROTATE==90)
            {
                ARMROTATE=150;
            }
            
            glutPostRedisplay();
            
            
            break;
            
    case 's':   //Aggressive/Passive Position
            
            TAIL_ROTATE+=50;
            
            if (TAIL_ROTATE>-20)
            {
                TAIL_ROTATE=-70;
            }
            
            glutPostRedisplay();
            
            break;
            
            
    case 'l':
            
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glutPostRedisplay();
            break;
            
    case 'k':
            
            glPolygonMode(GL_BACK, GL_FILL);
            glutPostRedisplay();
            break;
            
    case 'j': 
            
            glPolygonMode(GL_FRONT, GL_FILL);
            glutPostRedisplay();
            break;
    
            
    case 'a':   //animation
 
            iterations=50;  //reset the timer
            glutTimerFunc(TIME_INTERVAL, update,iterations);
            glutPostRedisplay();
            break;
            
            

	//ZOOM
	case 'z': G_zoom/=1.5; break;
	case 'Z': G_zoom*=1.5; 	break;

	//GRIDS
	case 'x': x_y_display++; if(x_y_display>1) x_y_display=0; break;
	case 'y': y_z_display++; if(y_z_display>1) y_z_display=0; break;
	case 'X': x_z_display++; if(x_z_display>1) x_z_display=0; break;

	default: printf("Unknown input");
	}
	glutPostRedisplay();

}


//======================================================
// MOUSE CALLBACK ROUTINES
//======================================================

void mouseMotionCallBack(int x, int y) 
{
	// Called when the Mouse is moved with left button down
	G_theta[0] = pitch0 + (y - mouseY0);
    G_theta[1] = yaw0 + (x - mouseX0);
	glutPostRedisplay();
} 

void mouseClickCallBack(int button, int state, int x, int y) 
{
	// Called on button press or release
    switch (state)
    {
		case GLUT_DOWN:
			MousePressed = true;
			pitch0 = G_theta[0]; 
			yaw0 = G_theta[1];
			mouseX0 = x; mouseY0 = y;
			break;
		default:
		case GLUT_UP:
			MousePressed = false;
			break;
    }
} 


//======================================================
// DISPLAY RESHAPE CALLBACK ROUTINE 
//======================================================
void reshapeCallBack(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		if (w <= h)
			glOrtho(-2.0, 2.0, -2.0 * (float) h / (float) w,
				2.0 * (float) h / (float) w, -10.0, 10.0);
		else
			glOrtho(-2.0 * (float) w / (float) h,
				2.0 * (float) w / (float) h, -2.0, 2.0, -10.0, 10.0);
    glMatrixMode(GL_MODELVIEW);
}


//======================================================
// DISPLAY CALLBACK ROUTINE 
//======================================================
void displayCallBack(void)
{
	// display callback,
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	viewControl();
    drawScene();
	glFlush();
	glutSwapBuffers();
}

//======================================================
// VIEW CONTROL ROUTINE 
//======================================================
void viewControl()
{
	// Reset matrix
	glLoadIdentity();

	//Rotate everything 
	glRotatef(G_theta[0], 1.0, 0.0, 0.0);
	glRotatef(G_theta[1], 0.0, 1.0, 0.0);
	glRotatef(G_theta[2], 0.0, 0.0, 1.0);

	//zoom (NB glOrtho projection)
	glScalef(G_zoom,G_zoom,G_zoom);
}

//======================================================
// MAIN PROGRAM 
//======================================================
int main(int argc, char **argv)
{
	// Create and Name window
	// Add Display & Mouse CallBacks
    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(1200, 800);
	glutCreateWindow("Blair & Josh's Scorpion");
	glutReshapeFunc(reshapeCallBack);
	glutDisplayFunc(displayCallBack);
	glutIdleFunc(NULL);
	glutMouseFunc(mouseClickCallBack);
    glutMotionFunc(mouseMotionCallBack);
	glutKeyboardFunc(keyboardCallBack);

	loadMenu();

	glClearColor(1.0, 1.0, 1.0, 1.0);
	glColor3f(1.0, 0.0, 0.0);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	glEnableClientState(GL_COLOR_ARRAY); 
	glEnableClientState(GL_VERTEX_ARRAY);
    glEnable(GL_DEPTH_TEST); /* Enable hidden--surface--removal */

	//Print Application Usage
	printf("Program Controls:\n");
	printf("Left Mouse Button & Drag - Changes the View.\n");
	printf("Key 'a' : Animate Scorpion.\n");
	printf("Key 's' : Change Tail Posture\n");
	printf("Key 'd' : Move Arms.\n");
	printf("Key 'f' : Move Claws.\n");
	printf("Key 'g' : Move Pincers.\n");
	printf("Key 'h' : Move Legs.\n");
	printf("Key 'j' : Front Fill.\n");
    printf("Key 'k' : Back Fill.\n");
    printf("Key 'l' : Wire Frame.\n");
	printf("Key 'z/Z' : Zooms view Out/In.\n");
	printf("Key 'x/y/X' : Toggles Grid for the X/Y/Z Axis.\n");
    
	glutMainLoop();
	return 0;
}

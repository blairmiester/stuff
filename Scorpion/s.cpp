
#include <stdlib.h>
//#include <GL/glut.h>
#include <glut/glut.h>
#include <math.h>
#include <stdio.h>
#include "3DCurve.h"
#include "cube.h"
#include "s.h"

//static double theta_stop1 = 90;

//======================================================
// DRAW_Letter_S
//======================================================
void drawLetterS()
{
    glPushMatrix();
    glTranslatef(0, 3.5, 0);
    
        glPushMatrix();
        glTranslatef(0,2.0,0.0); //Position of first curve

        //Draw curve using code in 3DCurve.cpp
        draw3Dcurve  (1.0,                      //Depth  
                      1.5,                      //Inner radius
                      2.0,                      //Outer radius
                      0,                      //Start angle
                      270,              //Stop angle
                      5.0);                     //Anular increments
        glPopMatrix();
    
        glPushMatrix();
        glTranslatef(0,-1.5,0.0); //Position of second curve
        glRotatef(180,0,0,1);
        
        draw3Dcurve  (1.0,                      //Depth  
                      1.5,                      //Inner radius
                      2.0,                      //Outer radius
                      0,                      //Start angle
                      270,              //Stop angle
                      5.0);                     //Anular increments
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(2.0,2.0,0.0); //Place rectangle 1 here.
        glScalef(1.0,0.5,1.0);
        cube();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(-2,-1.5,0.0); //Place rectangle 2 here.
        glScalef(1.0,0.5,1.0);
        cube();
        glPopMatrix();
    
    glPopMatrix();

}

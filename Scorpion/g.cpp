
#include <stdlib.h>
//#include <GL/glut.h>
#include <glut/glut.h>
#include <math.h>
#include <stdio.h>
#include "3DCurve.h"
#include "cube.h"
#include "g.h"

static double theta_stop1 = 90;

void drawLetterG()
{
    glPushMatrix();
    glTranslatef(0, 7, 0);
    
        glPushMatrix();
        glTranslatef(0,0,0); //Place rectangle 1 here.
        glScalef(4.0,0.5,1.0);
        cube();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(-1.75,-3.5,0.0); //Place rectangle 1 here.
        glScalef(0.5,7.20,1.0);
        cube();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(0.0,-7.0,0.0); //Place rectangle 1 here.
        glScalef(4.0,0.5,1.0);
        cube();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(1.75,-5.5,0.0); //Place rectangle 1 here.
        glScalef(0.5,3.0,1.0);
        cube();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(1.0,-4.0,0.0); //Place rectangle 1 here.
        glScalef(2.0,0.5,1.0);
        cube();
        glPopMatrix();
    
    glPopMatrix();
}

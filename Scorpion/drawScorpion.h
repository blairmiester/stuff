//
//  drawScorpion.h
//  Scorpion
//
//  Created by Blair Murray on 17/10/2012.
//  Copyright (c) 2012 Blair Murray. All rights reserved.
//

extern void drawScorpion(float PINCER_ONE_ROTATE, float PINCER_TWO_ROTATE, float CLAWROTATE, float ARMROTATE, float MOVELEG, float TAIL_ROTATE);
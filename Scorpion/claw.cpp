//
//  claw.cpp
//  Scorpion
//
//  Created by Blair Murray and Josh Stevenson on 17/10/2012.
//  Copyright (c) 2012 Blair Murray. All rights reserved.
//

#include <stdlib.h>
//#include <GL/glut.h>
#include <glut/glut.h>
#include <math.h>
#include <stdio.h>
#include "3DCurve.h"
//#include "cube.h"
#include "j.h"
#include "claw.h"
#include "pincer.h"

void drawClaw(float PINCER_ONE_ROTATE, float PINCER_TWO_ROTATE, float CLAWROTATE, float ARMROTATE);

void drawClaw(float PINCER_ONE_ROTATE, float PINCER_TWO_ROTATE, float CLAWROTATE, float ARMROTATE)
{
        
    //LEFT CLAW
    glPushMatrix();
    glTranslatef(3, -0.7, -1.5);
    glRotatef(-90, 1, 0, 0);
    glRotatef(ARMROTATE, 0, 0, 1);
    glScalef(0.5, 0.5, 0.5);
    drawLetterJ();
    
    
    
    //LEFT PINCER
    glTranslatef(-1.5, -7, 0);
    glRotatef(-30, 0, 0, 1);
    glScalef(0.9, 0.5, 0.9);
    drawPincer(PINCER_ONE_ROTATE, PINCER_TWO_ROTATE, CLAWROTATE);
    glPopMatrix();
    
    
    
    //RIGHT CLAW
    glPushMatrix();
    glTranslatef(3, -0.7, 1.5);
    glRotatef(90, 1, 0, 0);
    glRotatef(ARMROTATE, 0, 0, 1);
    glScalef(0.5, 0.5, 0.5);
    drawLetterJ();
    
    
    
    //RIGHT PINCER
    glTranslatef(-1.5, -7, 0);
    glRotatef(-30, 0, 0, 1);
    glScalef(0.9, 0.5, 0.9);
    drawPincer(PINCER_ONE_ROTATE, PINCER_TWO_ROTATE, CLAWROTATE);
    glPopMatrix();
    
}
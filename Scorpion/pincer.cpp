//
//  pincer.cpp
//  Scorpion
//
//  Created by Blair Murray and Josh Stevenson on 17/10/2012.
//  Copyright (c) 2012 Blair Murray. All rights reserved.
//

//#include "stdafx.h" WINDOWS
#include <stdlib.h>
//#include <GL/glut.h>
#include <GLUT/GLUT.h>
#include <math.h>
#include <stdio.h>
#include "l.h"
#include "m.h"

void drawPincer(float PINCER_ONE_ROTATE, float PINCER_TWO_ROTATE, float CLAWROTATE);

void drawPincer(float PINCER_ONE_ROTATE, float PINCER_TWO_ROTATE, float CLAWROTATE)
{
		glPushMatrix();
		glTranslatef(0.0,0.0,0.0);  //draw M at origin
        glRotatef(CLAWROTATE, 0, 0, 1);
		//glScalef();
		drawLetterM();
		
        
		glPushMatrix();
		glTranslatef(-2.0,-3.5,0.0);  //pincer one on left
        glRotatef(PINCER_ONE_ROTATE, 0, 0, 1);    //KEEP FOR PINCER ACTION
		//glScalef();
		drawLetterL();
		glPopMatrix();
		
		glPushMatrix();
		glTranslatef(2.0,-3.5,0.0); //pincer two on right rotate
        glRotatef(PINCER_TWO_ROTATE, 0, 0, 1);   //KEEP FOR PINCER ACTION
        glScalef(-1.0,1.0,1.0); //inverts the L
		drawLetterL();
		glPopMatrix();
    
		glPopMatrix();
		
}

//
//  drawScorpion.cpp
//  Scorpion
//
//  Created by Blair Murray and Josh Stevenson on 17/10/2012.
//  Copyright (c) 2012 Blair Murray. All rights reserved.
//

#include <stdlib.h>
//#include <GL/glut.h>
#include <glut/glut.h>
#include <math.h>
#include <stdio.h>
#include "body.h"
#include "l.h"
#include "claw.h"
#include "drawScorpion.h"
#include "tail.h"


void drawScorpion(float PINCER_ONE_ROTATE, float PINCER_TWO_ROTATE, float CLAWROTATE, float ARMROTATE, float MOVELEG, float TAIL_ROTATE);

void drawScorpion(float PINCER_ONE_ROTATE, float PINCER_TWO_ROTATE, float CLAWROTATE, float ARMROTATE, float MOVELEG, float TAIL_ROTATE)
{
    glPushMatrix();
    
        drawBody();         //Draw the body of scorpion
    
        drawClaw(PINCER_ONE_ROTATE, PINCER_TWO_ROTATE, CLAWROTATE, ARMROTATE);      //Draw the scorpions Arms, Claws and Pincers
    
        glPushMatrix();
            glTranslatef(-3, 0, 0);
            glRotatef(180, 0, 1, 0);
            drawTail(TAIL_ROTATE);          //Draw tail and put in correct position
        glPopMatrix();
    
        glPushMatrix();
            glTranslatef(0, -1, -1);
            glScalef(0.7, 0.6, 0.7);
            glRotatef(90, 1, 0, 0);
            glRotatef(-90, 0, 1, 0);
            glRotatef(MOVELEG, 1, 0, 0);    //Variable for moving leg
            drawLetterL();                  //Draw an L to be a leg
        glPopMatrix();
    
        glPushMatrix();
            glTranslatef(-2, -1, -1);
            glScalef(0.7, 0.6, 0.7);
            glRotatef(90, 1, 0, 0);
            glRotatef(-90, 0, 1, 0);
            glRotatef(MOVELEG, 1, 0, 0);
            drawLetterL();
        glPopMatrix();
    
        glPushMatrix();
            glTranslatef(2, -1, -1);
            glScalef(0.7, 0.6, 0.7);
            glRotatef(90, 1, 0, 0);
            glRotatef(-90, 0, 1, 0);
            glRotatef(MOVELEG, 1, 0, 0);
            drawLetterL();
        glPopMatrix();
    
        glPushMatrix();
            glTranslatef(-2, -1, 1);
            glScalef(0.7, 0.6, 0.7);
            glRotatef(-90, 1, 0, 0);
            glRotatef(90, 0, 1, 0);
            glRotatef(-MOVELEG, 1, 0, 0);
            drawLetterL();
            glPopMatrix();
    
        glPushMatrix();
            glTranslatef(0, -1, 1);
            glScalef(0.7, 0.6, 0.7);
            glRotatef(-90, 1, 0, 0);
            glRotatef(90, 0, 1, 0);
            glRotatef(-MOVELEG, 1, 0, 0);
            drawLetterL();
        glPopMatrix();
    
        glPushMatrix();
            glTranslatef(2, -1, 1);
            glScalef(0.7, 0.6, 0.7);
            glRotatef(-90, 1, 0, 0);
            glRotatef(90, 0, 1, 0);
            glRotatef(-MOVELEG, 1, 0, 0);
            drawLetterL();
        glPopMatrix();
    
    glPopMatrix();
}

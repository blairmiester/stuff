//
//  tail.cpp
//  Scorpion
//
//  Created by Blair Murray and Josh Stevenson on 17/10/2012.
//  Copyright (c) 2012 Blair Murray. All rights reserved.
//


//#include "stdafx.h" WINDOWS
#include <stdlib.h>
#include <glut/glut.h>
#include <math.h>
#include <stdio.h>
#include "l.h"
#include "g.h"
#include "s.h"

void drawTail(float TAIL_ROTATE);

void drawTail(float TAIL_ROTATE)
{
    glPushMatrix();
    glRotatef(TAIL_ROTATE, 0, 0, 1);
    glScalef(0.5,0.5,0.5);
    drawLetterG();
	
    glPushMatrix();
    glTranslatef(0.0,7.0,0.0);
    glRotatef(180, 0, 1, 0);
    glRotatef(-30, 0, 0, 1);
    //glScalef(0.5, 0.5, 0.5);
    drawLetterS();
   
    glTranslatef(6.5,5.5,0);
    glRotatef(270, 0, 0, 1);
    //glRotatef(180, 0, 1, 0);
    glScalef(0.5, 1, 0.5);
    drawLetterL();
    glPopMatrix();
    
    glPopMatrix();
}

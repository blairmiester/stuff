import java.util.*;
import java.io.*;

public class triangles 
{

public static void main(String[] args) throws FileNotFoundException, IOException
{

File results = new File("results.txt");
BufferedWriter output = null;
output = new BufferedWriter(new FileWriter(results));

int nTriangles;
double sidea;
double sideb;
double sidec;
int i = 1;


Scanner scanner = new Scanner(new File("test.txt"));
ArrayList<Integer> tri = new ArrayList<Integer>();


//put files from input into an arraylist
while(scanner.hasNextInt())
{
   tri.add(scanner.nextInt());
}

//retrieve how many triangles are in the input file
nTriangles = tri.get(0);

//check to see if the input file has the correct format
if (nTriangles * 3 != tri.size() - 1)
{
	System.out.println("Input File Wrong");
	output.write("Input File INVAILD");
	System.exit(0);
}

System.out.println("There are " + nTriangles + " triangle(s) in the file");
System.out.println();


while(nTriangles>0)
{
	//set sides of triangle to calculate
	sidea = tri.get(i++);
	sideb = tri.get(i++);
	sidec = tri.get(i++);
	
	output.write(String.valueOf(sidea));
	output.write(" ");
	output.write(String.valueOf(sideb));
	output.write(" ");
	output.write(String.valueOf(sidec));
	output.write(" ");
	

System.out.println("Sides" + " " + sidea + " " + sideb + " " + sidec);

//Check if its a triangle
if (!(((sidea + sideb) >= (sidec)) && ((sidea + sidec) >= (sideb)) && ((sideb + sidec) >= (sidea))))
{																											//DECISION POINT 1
	System.out.println("not a triangle");
	System.out.println();
	output.write("NOT A TRIANGLE");
	output.newLine();
	nTriangles --;
	//NOT A TRIANGLE
}

//is it right angled
else if ((((sidea * sidea) + (sideb * sideb)) == (sidec * sidec))||(((sidea * sidea) + (sidec * sidec)) == (sideb * sideb))||(((sidec * sidec) + (sideb * sideb)) == (sidea * sidea)))
{
	System.out.println("Isoceles-Right Angle");
	output.write("Isoceles-Right Angle");																	//DECISION POINT 2
	output.write(" ");
	findAngle(sidea, sideb, sidec, output);
	output.newLine();
	nTriangles --;
}


//is it equilateral
else if ((sidea == sideb) && (sidea == sidec) && (sideb == sidec))	
{																											//DECISION POINT 3
	System.out.println("equilateral");
	output.write("equilateral");
	output.write(" ");
	findAngle(sidea, sideb, sidec, output);
	output.newLine();
	nTriangles --;
}

//is is isoceles
else if (((sidea == sideb) || (sideb == sidec) || (sidea == sidec)) && !((sidea == sideb) && (sideb == sidec) && (sidea == sidec)))
{
	System.out.println("isosceles");
	output.write("isoceles");
	output.write(" ");
	findAngle(sidea, sideb, sidec, output);
	output.newLine();
	nTriangles --;
}


//it must be scalene
else if (!((sidea == sideb) && (sidea == sidec) && (sideb == sidec)))
{
	System.out.println("scalene");
	output.write("scalene");
	output.write(" ");
	findAngle(sidea, sideb, sidec, output);
	output.newLine();
	nTriangles --;
}

}
output.close();
}


public static void findAngle(double sidea, double sideb, double sidec, BufferedWriter output) throws IOException
{
	
double max = 0;

double angleA = Math.toDegrees(Math.acos((sidea * sidea + sideb * sideb - sidec * sidec) / (2 * sidea * sideb)));
double angleB = Math.toDegrees(Math.acos((sidea * sidea + sidec * sidec - sideb * sideb) / (2 * sidea * sidec)));
double angleC = Math.toDegrees(Math.acos((sideb * sideb + sidec * sidec - sidea * sidea) / (2 * sideb * sidec)));


//find max angle of triangle
if (angleA >= angleB && angleA >= angleC) 
{
	max = angleA;
}
else if (angleB >= angleA && angleB >= angleC)
{
	max = angleB;
}
else if (angleC >= angleA && angleC >= angleB)
{
	max = angleC;
}


//if 90, right angle
if (max == 90)
{
	System.out.println("Right Angled");
	output.write("Right");
	System.out.println();
}

//if less than 90, acute
else if(max < 90)
{
	System.out.println("Acute");
	output.write("Acute");
	System.out.println();
}

//if more than 90, obtuse
else if(max > 90)
{
	System.out.println("Obtuse");
	output.write("Obtuse");
	System.out.println();
}
}


}
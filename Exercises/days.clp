(deffacts MAIN::t1
   (Today is a weekend))

(deffacts MAIN::t2
   (Today is a weekday))

(defrule MAIN::r1
   (Today is a weekend)
   =>
   (assert (then stay in bed)))

(defrule MAIN::r2
   (Today is a weekday)
   =>
   (assert (then go to school)))


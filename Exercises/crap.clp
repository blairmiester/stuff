(deffacts MAIN::f1
   (no_beard))

(deffacts MAIN::f2
   (beard))

(deffacts MAIN::f3
   (big_beard))

(defrule MAIN::no_beard
   (length ?t)
   (test (= ?t 0))
   =>
   (assert (is a phone salesman)))

(defrule MAIN::beard
   (length ?t)
   (test (>= ?t 12))
   =>
   (assert (is philosopher)))

(defrule MAIN::big_beard
   (length ?t)
   (test (> ?t 12))
   =>
   (assert (is god)))


package backend;

import java.util.*;
import json.*;
import wattball.*;

/**
* This class deals with all of the scheduling for the Hurdle events.
* @author Josh Stevenson 
* @version 1.4.3
* 
*/

public class HurdlingScheduler {

	    /**
	    * Here we have the function that we call from the wattball package to 
	    * connect to the website and select the required data. 
	    */
	
	private WattballInterop connection;
	
	public HurdlingScheduler() {
		
		connection = new WattballInterop();
	}

		/**
	    * This function is where all of the data is extracted and placed into specified
	    * ArrayLists to be sorted into sub categories. These are then stored into strings
	    * which are then combined together with the desired output format to POST to the 
	    * add index pages of the Wattball website.
	    */
	
public boolean schedule() {
		
		connection = new WattballInterop();

		ArrayList<JSONObject> allHurdlers = connection.getModelList("Trackrunner", "Trackrunners");

		ArrayList<JSONObject> maleHurdlers = new ArrayList<JSONObject>(); 
		ArrayList<JSONObject> femaleHurdlers = new ArrayList<JSONObject>(); 
		
		ArrayList<JSONObject> noTimeMales = new ArrayList<JSONObject>();  //These are day One
		ArrayList<JSONObject> noTimeFemales = new ArrayList<JSONObject>();  //These are day One

		ArrayList<JSONObject> day2Males = new ArrayList<JSONObject>(); 
		ArrayList<JSONObject> day2Females = new ArrayList<JSONObject>();
		
		ArrayList<JSONObject> day3Males = new ArrayList<JSONObject>(); 
		ArrayList<JSONObject> day3Females = new ArrayList<JSONObject>();
		
		ArrayList<JSONObject> day4Males = new ArrayList<JSONObject>(); 
		ArrayList<JSONObject> day4Females = new ArrayList<JSONObject>();
		
		ArrayList<JSONObject> day5Males = new ArrayList<JSONObject>(); 
		ArrayList<JSONObject> day5Females = new ArrayList<JSONObject>();
		
		
		for(int z = 0; z < allHurdlers.size(); z++) {

			if(allHurdlers.get(z).getString("gender").equals("M")){
				
				maleHurdlers.add(allHurdlers.get(z));
			}
		}	
		
		for(int z = 0; z < allHurdlers.size(); z++) {

			if(allHurdlers.get(z).getString("gender").equals("F")){
				
				femaleHurdlers.add(allHurdlers.get(z));
			}
		}	
		
		for(int z = 0; z < maleHurdlers.size(); z++) {

			if(maleHurdlers.get(z).getString("best_time").equals("0")){
				
				noTimeMales.add(maleHurdlers.get(z));
			}
		}
		
		for(int z = 0; z < femaleHurdlers.size(); z++) {

			if(femaleHurdlers.get(z).getString("best_time").equals("0")){
				
				noTimeFemales.add(femaleHurdlers.get(z));
			}
		}
		
		for(int z = 0; z < maleHurdlers.size(); z++) {

			day2Males.add(maleHurdlers.get(z));
		}
		
		for(int z = 0; z < femaleHurdlers.size(); z++) {

			day2Females.add(femaleHurdlers.get(z));
		}
		
		for(int z = 0; z < day2Males.size() / 2; z++) {

			day3Males.add(day2Males.get(z));
		}
		
		for(int z = 0; z < day2Females.size() / 2; z++) {

			day3Females.add(day2Females.get(z));
		}
		
		for(int z = 0; z < day3Males.size() / 2; z++) {

			day4Males.add(day3Males.get(z));
		}
		
		for(int z = 0; z < day3Females.size() / 2 ; z++) {

			day4Females.add(day3Females.get(z));
		}
		
		for(int z = 0; z < day4Males.size() / 2; z++) {

			day5Males.add(day4Males.get(z));
		}
		
		for(int z = 0; z < day4Females.size() / 2 ; z++) {

			day5Females.add(day4Females.get(z));
		}
		
		//########################DAY ONE TIMES#########################################//
		
		String id = "data[Trackheat][id]=";  
		String scheduleID = "&data[Trackheat][scheduleday_id]="; 
		String heatTime = "&data[Trackheat][heat_time]="; 
		String gender = "&data[Trackheat][gender]="; 
		String fieldNumber = "&data[Trackheat][field_number]="; 

		String firstQuotes = "\""; //Chucks in the quotes at front
		String lastQuotes = "\""; //Chucks in the quotes at back
		
		String hurdDay1M = ""; //Name of string for specified day and gender
		String dayOneID = "1"; // string for day number 
		
		int[] fieldNums = {1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 8};
		int[] finalFieldNums = {1, 2, 3, 4, 5, 6, 7, 8};
		
		
		for(int i = 0; i < noTimeMales.size(); i++) {
			
			if(noTimeMales.size() > 0){
				
					hurdDay1M += firstQuotes + id + noTimeMales.get(i).getString("id") + scheduleID + dayOneID + heatTime + 
					noTimeMales.get(i).getString("best_time") + gender + noTimeMales.get(i).getString("gender") +
					fieldNumber + fieldNums[i] + lastQuotes;
				
			}
		}
				
		String hurdDay1F = ""; 
		
		for(int i = 0; i < noTimeFemales.size(); i++) {
			
			if(noTimeFemales.size() > 0){
				
					hurdDay1F += firstQuotes + id + noTimeFemales.get(i).getString("id") + scheduleID + dayOneID + heatTime + 
					noTimeFemales.get(i).getString("best_time") + gender + noTimeFemales.get(i).getString("gender") +
					fieldNumber + fieldNums[i] + lastQuotes;
			}
		}
		
		//########################DAY TWO TIMES#########################################//
		
		String dayTwoID = "2"; 
		String hurdDay2M = ""; 

		
		for(int i = 0; i < day2Males.size(); i++) {
			
			if(day2Males.size() > 0){
				
					hurdDay2M += firstQuotes + id + day2Males.get(i).getString("id") + scheduleID + dayTwoID + heatTime + 
					day2Males.get(i).getString("best_time") + gender + day2Males.get(i).getString("gender") +
					fieldNumber + fieldNums[i] + lastQuotes;
			}
		}
		
		String hurdDay2F = ""; 
		
		for(int i = 0; i < day2Females.size(); i++) {
			
			if(day2Females.size() > 0){
				
					hurdDay2F += firstQuotes + id + day2Females.get(i).getString("id") + scheduleID + dayTwoID + heatTime + 
					day2Females.get(i).getString("best_time") + gender + day2Females.get(i).getString("gender") +
					fieldNumber + fieldNums[i] + lastQuotes;
			}
		}

		//########################DAY THREE TIMES#########################################//
		
		String dayThreeID = "3"; 
		String hurdDay3M = ""; 
		
		for(int i = 0; i < day3Males.size(); i++) {
			
			if(day3Males.size() > 0){
				
					hurdDay3M += firstQuotes + id + day3Males.get(i).getString("id") + scheduleID + dayThreeID + heatTime + 
					day3Males.get(i).getString("best_time") + gender + day3Males.get(i).getString("gender") +
					fieldNumber + fieldNums[i] + lastQuotes;
			}
		}
		
		String hurdDay3F = ""; 
		
		for(int i = 0; i < day3Females.size(); i++) {
			
			if(day3Females.size() > 0){
				
					hurdDay3F += firstQuotes + id + day3Females.get(i).getString("id") + scheduleID + dayThreeID + heatTime + 
					day3Females.get(i).getString("best_time") + gender + day3Females.get(i).getString("gender") +
					fieldNumber + fieldNums[i] + lastQuotes;
			}
		}
		//########################DAY FOUR TIMES#########################################//

		String dayFourID = "4"; 
		String hurdDay4M = ""; 
		
		for(int i = 0; i < day4Males.size(); i++) {
			
			if(day4Males.size() > 0){
				
					hurdDay4M += firstQuotes + id + day4Males.get(i).getString("id") + scheduleID + dayFourID + heatTime + 
					day4Males.get(i).getString("best_time") + gender + day4Males.get(i).getString("gender") +
					fieldNumber + fieldNums[i] + lastQuotes;
			}
		}

		String hurdDay4F = ""; 
		
		for(int i = 0, j = 0; i < day4Females.size(); i++, j++) {
			
			if(day4Females.size() > 0){
				
					hurdDay4F += firstQuotes + id + day4Females.get(i).getString("id") + scheduleID + dayFourID + heatTime + 
					day4Females.get(i).getString("best_time") + gender + day4Females.get(i).getString("gender") +
					fieldNumber + fieldNums[j] + lastQuotes;
					
			}
		}
		//########################DAY FIVE TIMES#########################################//

		String hurdDay5M = "";
		
		for(int i = 0, j = 0; i < day5Males.size(); i++, j++) {
			
			if(day5Males.size() > 0){
				
					hurdDay5M += firstQuotes + id + day5Males.get(i).getString("id") + scheduleID + dayFourID + heatTime + 
					day5Males.get(i).getString("best_time") + gender + day5Males.get(i).getString("gender") +
					fieldNumber + finalFieldNums[j] + lastQuotes;
					
			}
		}
		
		String hurdDay5F = "";

		
		for(int i = 0, j = 0; i < day5Females.size(); i++, j++) {
			
			if(day5Males.size() > 0){
				
					hurdDay5F += firstQuotes + id + day5Females.get(i).getString("id") + scheduleID + dayFourID + heatTime + 
					day5Females.get(i).getString("best_time") + gender + day5Females.get(i).getString("gender") +
					fieldNumber + finalFieldNums[j] + lastQuotes;
					
			}
		}
		/*
		
		for(int i = 0; i < maleHurdlers.size() - 1 ; i++) {
			
			malesTimes.add(maleHurdlers.get(i).getInt("best_time"));
			malesIDs.add(maleHurdlers.get(i).getInt("id"));
		}
		
		for(int i = 0; i < femaleHurdlers.size() - 1 ; i++) {
			
			malesTimes.add(femaleHurdlers.get(i).getInt("best_time"));
			malesIDs.add(femaleHurdlers.get(i).getInt("id"));
		}
*/
   /*
		for(int i = 0; i < maleHurdlers.size() - 1 ; i++) {
			
			for(int j = i + 1; j < maleHurdlers.size(); j++){

				if(maleHurdlers.get(i).getInt("best_time") > maleHurdlers.get(j).getInt("best_time")){
					
					final int k = 1 ; 

					Collections.rotate(maleHurdlers, k); 

				}
			}
		}
		/*
		 for (int i=0; i<x.length-1; i++) {
		        for (int j=i+1; j<x.length; j++) {
		            if (x[i] > x[j]) {
		                //... Exchange elements
		                int temp = x[i];
		                x[i] = x[j];
		                x[j] = temp;
		            }
		        }
		    }
		*//*
		for(int z = 0; z < femaleHurdlers.size(); z++) {

			if(femaleHurdlers.get(z).getString("best_time").equals("0")){
				
				day2TimesF.add(femaleHurdlers.get(z));
			}
		}


		/*	
			for(int z = 0; z < allHurdlers.size(); z++) {

			if(allHurdlers.get(z).getString("best_time").equals("0") && 
			   allHurdlers.get(z).getString("gender").equals("F") ){
				
				noTimeFemales.add(allHurdlers.get(z).getString("id"));
				noTimeFemales.add(allHurdlers.get(z).getString("email"));
				noTimeFemales.add(allHurdlers.get(z).getString("gender"));
				noTimeFemales.add(allHurdlers.get(z).getString("full_name"));
				noTimeFemales.add(allHurdlers.get(z).getString("best_time"));
			}
		}
		 */
		
		 System.out.println();                                           
		 System.out.println("### OUPUT ###");
		 System.out.println(hurdDay2F);
		 System.out.println();                                           
		 System.out.println("### OUTPUT OF ALL HURDLERS ###");
		 System.out.println(allHurdlers); 
		 System.out.println();
		 System.out.println("### OUTPUT OF ALL MALE HURDLERS ###");
		 System.out.println(maleHurdlers); 
		 System.out.println();
		 System.out.println("### OUTPUT OF ALL FEMALE HURDLERS ###");
		 System.out.println(femaleHurdlers); 
		 System.out.println();
		 System.out.println("### OUTPUT OF ALL MALE HURDLERS WITH NO TIME ### DAY ONE ###");
		 System.out.println(noTimeMales);
		 System.out.println();
		 System.out.println("### OUTPUT OF ALL FEMALE HURDLERS WITH NO TIME ### DAY ONE ###");
		 System.out.println(noTimeFemales);
		 System.out.println();
		 /*
		 System.out.println("### OUTPUT FOR DAY TWO MEN ###");
		 System.out.println(day2Males);
		 System.out.println();
		 System.out.println("### OUTPUT FOR DAY TWO WOMEN ###");
		 System.out.println(day2Females);
		 System.out.println();
		 System.out.println("### OUTPUT FOR DAY THREE MEN ###");
		 System.out.println(day3Males);
		 System.out.println();
		 System.out.println("### OUTPUT FOR DAY THREE WOMEN ###");
		 System.out.println(day3Females);
		 System.out.println();
		 System.out.println("### OUTPUT FOR DAY FOUR MEN ###");
		 System.out.println(day4Males);
		 System.out.println();
		 System.out.println("### OUTPUT FOR DAY FOUR WOMEN ###");
		 System.out.println(day4Females);
		 System.out.println();
		 System.out.println("### OUTPUT FOR DAY FIVE MEN ###");
		 System.out.println(day5Males);
		 System.out.println();
		 System.out.println("### OUTPUT FOR DAY FIVE WOMEN ###");
		 System.out.println(day5Females);
		 System.out.println();
		 */
		return false;
	}
}

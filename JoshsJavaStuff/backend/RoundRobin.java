//=============================================
//ROUND ROBIN IMPLEMENTATION - JOSH STEVENSON =
//=============================================

package backend;

import java.io.*;
//import java.net.*;
//import java.nio.charset.Charset;  //Import the usual libraries from Java 
import java.util.ArrayList;

import json.JSONArray;

import json.*;              //Package import to handle JASON scheduling


//####################################################################
//   I'VE KEPT THE OLD DATA COMMENTED IN THE CODE IN CASE OF         #
//   PROBLEMS WITH THE NEW. I'VE ALSO MADE IT AS CLEAR AS POSSIBLE   #
//   ON AS TO WHAT IS GOING ON. IF THERE IS A PROBLEM, JUST WRITE    #
//   A COMMENT IN THE CODE AND I'LL SORT IT AS SOON AS POSSIBLE      #
//####################################################################

class RoundRobin{

	//public static int[] rotateA(int[] a) {
	public static String[] rotateA(String[] a) {
		
			//STORES INITIAL ARRAY IN TEMPORARY VARIABLE
			String teams1 = a[0];
			int i;
			//int Array = a[0];
			//int i;
			
			//for (i = 0; i < a.length - 1; i++) {
			
			for (i = 0; i < a.length -1; i++) {
				// MOVE EACH ITEM UP ONE SPOT
				a[i] = a[i]; //+ 1];
				
				//a[i] = a[i + 1];
			//}
				
			}
			// AT END OF ARRY PLACE WHAT WAS STORED.
			//a[a.length -1] = Array;
			
		      a[a.length ] = teams1;
			
			// PRINT ARRAYS ELEMENTS
			//return (a);
			
			  return (a);
	}
	
	//public static int[] rotateB(int[] b) {
	public static String[] rotateB(String[] b) {
		
		//STORES INITIAL ARRAY IN TEMPORARY ARRAY
		//int Array1 = b[0];
		//int i;
		String teams2 = b[0];
		int i;
		
		//for (i = 0; i < b.length - 1; i++) {
		for (i = 0; i < b.length -1; i++) {
			// MOVE EACH ITEM UP ONE SPOT
			//b[i] = b[i + 1];
			b[i] = b[i + 1];
			
		//}
		}
		// AT END OF ARRY PLACE WHAT WAS STORED.
		//b[b.length -1] = Array1;
		b[b.length -1] = teams2;
		
		// PRINT ARRAYS ELEMENTS
		return (b);
}

	public static void main(String[] args) {
	  
		String teams1[] = new String[6];  //Team1 String Object
		String teams2[] = new String[6];  //Team2 String Object
		
		String[] team1Data = {"Eagles","Bats","Zombies","Frying Pans","Jumpers","Coat Hangers"};  //Names of teams go in here.
		String[] team2Data = {"Knives","Kittens","Lollygaggers","Crumpets","Plungers", "Crisps"};  //Names of teams go in here.
	
		   											
	//	try{
			
		//	JSONArray jsonarray = JsonReader.readJSONToArray();
		//	ArrayList<Object> Wattteams = getTeamsFromJSONArray(jsonarray);
			
	//	} catch (JSONException e){} catch (IOException e){}
		
		//  JSONArray jArray = JsonReader;
		
		
		for(int i = 0; i < teams1.length; i++) {
			
			 teams1[i] = new String( team1Data[i]);  //Populate team1 with info
		}
            
		for(int i = 0; i < teams2.length; i++) {
			
             teams2[i] = new String(team2Data[i]);  //Populate team2 with info
		}
		
			//int[] Array = {2, 3, 4, 5, 6, 1};    //Test Arrays
			//int[] Array1 = {12, 11, 10, 9, 8, 7};
			
			//for (int i = 0; i < Array.length && i < Array1.length; i++) {
		for (int i = 0; i < teams1.length && i < teams2.length; i++) {
				
				//rotateA(Array);             //Test Method Calls
				//rotateB(Array1);
				//printArray(Array, Array1);
				
				//rotateA(Teams1);
				rotateB(teams2);
				printArray(teams1, teams2);
				
			//}
		}

	}
		
	// PRINTS EACH ELEMENT OF THE ARRAY. NOT THE ARRAY ITSELF.
	//public static void printArray(int[] a, int[] b) {
	public static void printArray(String[] a, String[] b) { 
		
			for (int i = 0; i < a.length && i < b.length; i++) {
				
					System.out.print(a[i] + " V.S. " + b[i] + "| "); 
					//System.out.print(b[i] + " V.S. ");
			}
					
					System.out.println();
					System.out.println();
	
	}
	
	public static ArrayList<Object> getTeamsFromJSONArray(JSONArray jsonarray) throws IOException, JSONException {
		
		
		  ArrayList<Object> teams = new ArrayList<Object>(); //Create Array list to store JSON objects

		  for(int i = 0; i < jsonarray.length(); i++){ //Loop until all objects in Array are printed 
	    		
	    		System.out.println(jsonarray.getJSONObject(i).getJSONObject("Wattballteam").getString("team_name"));  //Uses getJSONObject function to retrieve separate
	    																				  //elements in the Array using i, the next method call
	    																				 //shows all the data in each of the Wattballteam objects
	    																				//this then gets filtered again to show only the team names
	    		                                                    
	    		teams.add(jsonarray.getJSONObject(i).getJSONObject("Wattballteam").getString("team_name"));   //Adds these to the ArrayList to be used in RoundRobin

	    	}
		  

		  		  
		  return teams;
		
	}
	
}

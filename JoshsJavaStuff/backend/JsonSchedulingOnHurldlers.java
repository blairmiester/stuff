/*package backend;

import java.io.BufferedReader;
import java.io.IOException;          //Supply required libraries
import java.io.InputStream;         
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

import json.JSONArray;

import json.*;

//==================================================//
//JASON CONNECTION IMPLEMENTATION                  =//
//==================================================//

public class JsonSchedulingOnHurldlers {
	  
	//####################################################################
	//   I'VE KEPT THE OLD DATA COMMENTED IN THE CODE IN CASE OF         #
	//   PROBLEMS WITH THE NEW. I'VE ALSO MADE IT AS CLEAR AS POSSIBLE   #
	//   ON AS TO WHAT IS GOING ON. IF THERE IS A PROBLEM, JUST WRITE    #
	//   A COMMENT IN THE CODE AND I'LL SORT IT AS SOON AS POSSIBLE      #
	//####################################################################

		  public static void main(String[] args) throws IOException, JSONException {		
			
			JSONArray jsonarray = JsonReader.readJSONToArray();
			
			ArrayList<Object> wattTeams = getTeamsFromJSONArray(jsonarray); //Create Array list to store JSON objects
						
			ArrayList<Object> teams1Data = new ArrayList<Object>(); //Has the JSON data to feed into teamSet1
			ArrayList<Object> teams2Data = new ArrayList<Object>(); //Has the JSON data to feed into teamSet2

			ArrayList<Object> rotatedTeams1Data = new ArrayList<Object>(); //Has the teamSet1 data to feed into completeScheduledSet
			ArrayList<Object> rotatedTeams2Data = new ArrayList<Object>(); //Has the teamSet2 data to feed into completeScheduledSet

			ArrayList<Object> completeScheduledSet = new ArrayList<Object>(); //Contains ENTIRE schedule for WattTeams

		    	
		    	for(int i = 0; i < jsonarray.length(); i++){ //Loop whilst going through the whole ArrayList for teams
		    		
		    		if(i <= jsonarray.length() / 2 - 1){ //First half of the list goes in first array
		    		
		    			teams1Data.add(jsonarray.getJSONObject(i)); //Add to array one
		    		}
		    		else {    //Second half goes in second array
		    		
		    			teams2Data.add(jsonarray.getJSONObject(i)); //Add to array two
		    		}
		    		
		    	}
		    	
		    		for(int a = 0; a < teams1Data.size(); a++) { //Loop through the ArrayList until the end

		    			rotatedTeams1Data.addAll(teams1Data); //Duplicate list by the size of ArrayList
		    			
		    		}
		    	
			    	for(int b = 0; b < teams2Data.size(); b++) { //Loop through the ArrayList until the end
					       
						final int j = 1 ;    //Constant variable to rotate through ArrayList only once each time
						
						Collections.rotate(teams2Data, j); //Use Collections.rotate function to go through ArrayList systematically	  
					        	
						//System.out.println("### OUTPUT OF ROTATED TEAMS2DATA SET ###");
						//System.out.println(teams2Data);
	
						rotatedTeams2Data.addAll(teams2Data); //Adds all rotated elements into one array
						
					    }
			    	
			    	for(int c = 0; c < teams1Data.size() && c < teams2Data.size(); c++) { //Loop through the ArrayList until the end

			    		completeScheduledSet.add(rotatedTeams1Data.get(c));
			    		completeScheduledSet.add(rotatedTeams2Data.get(c));

			    	}
		    	 	
		     System.out.println();                                           
			 System.out.println("### OUTPUT OF WATTTEAMS ###");
			 System.out.println(wattTeams); //Output full ArrayList to console
			 System.out.println();
			 System.out.println("### OUTPUT OF TEAMS1DATA ###");
			 System.out.println(teams1Data);
			 System.out.println();
			 System.out.println("### OUTPUT OF TEAMS2DATA ###");
			 System.out.println(teams2Data);
			 System.out.println();
			 System.out.println("### OUTPUT OF ROTATED TEAMS1DATA ###");
			 System.out.println(rotatedTeams1Data);
			 System.out.println();
			 System.out.println("### OUTPUT OF ROTATED TEAMS2DATA ###");
			 System.out.println(rotatedTeams2Data);
			 System.out.println();
			 System.out.println("### OUTPUT OF COMPLETE SCHEDULED SET ###");
			 System.out.println(completeScheduledSet);
			 System.out.println();


						 
	         
			
								
		}
		
		public static ArrayList<Object> getTeamsFromJSONArray(JSONArray jsonarray) throws IOException, JSONException {
			
			
			  ArrayList<Object> Xteams = new ArrayList<Object>(); //Create Array list to store JSON objects

			 // System.out.println("### getTeamsFromJSONArray OUTPUT ###");
			  
			  for(int i = 0; i < jsonarray.length(); i++){ //Loop until all objects in Array are printed 
		    		
		    		//System.out.println(jsonarray.getJSONObject(i).getJSONObject("Wattballteam").getString("team_name"));  //Uses getJSONObject function to retrieve separate
		    																				  //elements in the Array using i, the next method call
		    																				 //shows all the data in each of the Wattballteam objects
		    																				//this then gets filtered again to show only the team names
		    		                                                    
		    		Xteams.add(jsonarray.getJSONObject(i).getJSONObject("Wattballteam").getString("team_name"));   //Adds these to the ArrayList to be used in RoundRobin

		    	}
			  		  
			  return Xteams;
			
		}
	  
}
*/
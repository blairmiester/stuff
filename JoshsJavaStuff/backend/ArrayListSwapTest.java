package backend;

import java.util.*;

public class ArrayListSwapTest {

	public static void main(String[] args) {

		 List<Object> list = new ArrayList<Object>();
		 
		    list.add("1");
		    list.add("2");
		    list.add("3");
		    list.add("4");


			for(int i = 0; i < list.size(); i++) { //Loop through the ArrayList until the end
			       
				final int j = 1 ;    //Constant variable to rotate through ArrayList only once each time
				
				Collections.rotate(list, j); //Use Collections.rotate function to go through ArrayList systematically	  
			        				
				System.out.println(list); //Print out ArrayList to see if it works

			    }
			    
	}
}



	


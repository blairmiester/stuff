/**
 * 
 * @author Bjoern Guenzel - http://blog.blinker.net
 * 
 * Edited by Blair Murray for use with AI Assignment
 */
public class TravelingSalesman 
{

	private double[][] costs;
	
	private double[][] createMatrix = 
	  {
		//   E Ab  Ay FW  G  Inv St S
			{0,129,79,131,43,154,50,36}, 		// Edinburgh
			{129,0,179,157,146,105,79,119}, 	// Aberdeen
			{79,179,0,141,33,207,118,64}, 		// Ayr
			{131,157,141,0,116,74,134,96}, 		// Fort William
			{43,146,33,116,0,175,81,27}, 		// Glasgow
			{154,105,207,64,175,0,144,143}, 	// Inverness
			{50,79,118,134,81,144,0,52}, 		// St Andrews
			{36,119,64,96,27,143,52,0} 			// Stirling
	  };
	
	public int n;

	boolean[] visited;

	
	public TravelingSalesman(int n)
	{
		this.n = n;
		
		visited = new boolean[n];
		costs = new double[n][n];
		costs=createMatrix;
	}


	public double calculateCosts(int[] route){
		return calculateCosts(route, false);
	}
	
	public double calculateCosts(int[] route, boolean isVerbose){
		
		double travelCosts = 0;
		for (int i = 1; i < route.length; i++) {
			travelCosts += costs[route[i-1]][route[i]]; 
			
			if(isVerbose){
				System.out.println("costs from "+route[i-1]+" to "+route[i]+": "+costs[route[i-1]][route[i]]);
			}
			
		}

		//return to starting city
		travelCosts += costs[route[n-1]][route[0]];
		if(isVerbose){
			System.out.println("costs from "+route[n-1]+" to "+route[0]+": "+costs[route[n-1]][route[0]]);
		}
		return travelCosts;
	}
	
	public void printRoute(int[] route){
		for(int i = 0;i<route.length;i++){
			System.out.print(route[i]+" ");
		}
	}
	
	public void printCosts(){
		System.out.println("costs matrix for the traveling salesman problem:");
		for(int i = 0;i<costs.length;i++){
			for(int j = 0;j<costs[i].length;j++){
				System.out.print(costs[i][j]+" ");
			}
			System.out.print("\n");
		}
	}
	
}
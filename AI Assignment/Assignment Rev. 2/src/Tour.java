public class Tour extends TravelingSalesman implements Comparable<Object>		
// Comparable is for the ArrayList to sort the paths to find the smallest.
   {  
	  int[] path;       // paths for the cities
      int   dist;		// city distances

      public Tour(int[] vect)
      {  dist = matrixWeight[vect[n-1]][vect[0]]; // Start with return edge
      
         for (int k = 1; k < n; k++)   // Add in all the others
            dist += matrixWeight[vect[k-1]][vect[k]];
         path = new int[n];            // Deep copy of the vector
         System.arraycopy(vect, 0, path, 0, n);
      }

      public int compareTo ( Object o )
      {  
    	  return this.dist - ((Tour)o).dist;  
      }

      // For debugging convenience:  show the current state.
      public String toString()
      {  
    	  StringBuilder val = new StringBuilder(city[path[0]]);
         for ( int k = 1; k < n; k++ )
            val.append(", " + city[path[k]]);
         val.append(", " + city[path[0]]);
         val.append( String.format(" for %d", dist) );
         return val.toString();
      }
   }
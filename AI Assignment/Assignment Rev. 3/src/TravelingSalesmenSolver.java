import java.util.*;
import java.io.*;


public class TravelingSalesmenSolver
{
	
	public static String getArgument(String[] args, String arg){
		for(int i=0;i<args.length;i++){
			int index = args[i].toLowerCase().indexOf(arg.toLowerCase()+"=");
			if(index==0){
				return args[i].substring(index+arg.length()+1,args[i].length());
			}
		}
		return null;
	}
   
   public static void main (String[] args)
   {
	    String file = getArgument(args,"file");
		String search = getArgument(args,"search");
	   
      
	   if(file!=null)
	   {
		   
		   try{
			   
			   if(search!=null)
			   {
	   
				if (search.equals("backtrack"))
	   			{
		    		System.out.println("*** running backtrack algorithm ***");
					String filename = file;
 
     
					Scanner inp = new Scanner (new java.io.File(filename));

					System.out.println("The data was taken from " + filename);
					Backtrack.init(inp);
					Backtrack.tourPaths();
     
					Backtrack.printTour();
	   			}
	   
	   			else if(search.equals("genetic"))
	   			{
		    		TravelingSalesman salesman = new TravelingSalesman(8);
			
					salesman.printCosts();
					System.out.println("*** running genetic algorithm algorithm ***");
					Enviroment environment = new Enviroment(salesman);

					environment.run();
	   			}
			 
			   }
			   else
	   			{
		   			System.out.println("Error not a valid algorithm");
		   			return;
	   			}
			   
	
	   }
	   catch(FileNotFoundException e)
	   {
		   System.out.println("Error:   File Not Found");
	   }
	   }
	   else
	   {
				System.out.println("Error:   File not found");			
	   }
	   
   }
   
}
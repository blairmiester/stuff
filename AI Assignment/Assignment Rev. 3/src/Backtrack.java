import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;


public class Backtrack 
{
	static String tourStart = "Edinburgh";
	   static int[][]  matrixWeight;                 // Matrix for the weight of the edges
	   static String[] cityName;               // Array for the names of the cities
	   static int      n;                  // Dimension for weight and cityName
	   static ArrayList<Tour> path = new ArrayList<Tour>();
	   static int      bestTour = Integer.MAX_VALUE;
	   static boolean  tourDecision   =  true;    // is a known tour?
	   static boolean  tourFound =  true;    // is a tour found?
	   static boolean  tourKnown    =  true;    // for removing tours larger than known

	   // For debugging convenience:  show state UP TO this index
	   private static void partial(int[] vect, int index)
	   {  
		   
		   int dist = index < n-1 ? 0 : matrixWeight[vect[n-1]][vect[0]];
		   //int dist = matrixWeight[vect[n-1]][vect[0]];
		   

	      System.out.print (cityName[vect[0]]);
	      for ( int k = 1; k <= index; k++ )
	      {  System.out.print (", " + cityName[vect[k]]);
	         dist += matrixWeight[vect[k-1]][vect[k]];
	      }
	      System.out.println (" for distance " + dist);
	   }

	   // Initialise the global variables based on the file passed through
	   // the Scanner inp.  See the header documentation for the
	   // specifications for the input file.
	   public static void init(Scanner inp)
	   {  
		   int sub1, sub2;
	      String line;

	      n = inp.nextInt();
	      matrixWeight = new int[n][n];
	      cityName = new String[n];
	      // Initially, there are NO edges; hence -1.
	      for ( sub1 = 0; sub1 < n; sub1++ )
	      {
	         Arrays.fill(matrixWeight[sub1], -1);
	      }

	      inp.nextLine();   // Discard rest of first line
	      for ( sub1 = 0; sub1 < n; sub1++ )
	         cityName[sub1] = inp.nextLine();
	      Arrays.sort(cityName);     // Just to be sure (binarySearch)

	      inp.nextLine();   // Discard blank spacing line;

	      while ( inp.hasNext() )
	      {  
	    	  int    head, tail;
	         int    dist;
	         String src, dst;

	         line = inp.nextLine();   // E.g.:  "Aberdeen" "Glasgow" 146
	         // Remove the quotes
	         head = line.indexOf('"') + 1;
	         tail = line.indexOf('"', head);
	         src = line.substring(head, tail);

	         head = line.indexOf('"', tail+1) + 1;
	         tail = line.indexOf('"', head);
	         dst = line.substring(head, tail);

	         dist = Integer.parseInt( line.substring(tail+1).trim() );
	         sub1 = Arrays.binarySearch(cityName, src);
	         sub2 = Arrays.binarySearch(cityName, dst);
	         matrixWeight[sub1][sub2] = matrixWeight[sub2][sub1] = dist;
	      }
	   }

	   // Public access for generating the tours.
	   // Generate the initial path vector, then call recursive tour
	   public  static void tourPaths()
	   {
	      int[] vect = new int[n];
	      int   start;

	      // First arrangement vector.
	      for ( int k = 0; k < n; k++ )
	         vect[k] = k;
	      
	      //Here we have set tourStart to Edinburgh but this can be changed
	      start = Arrays.binarySearch(cityName, tourStart);
	      if ( start >= 0 )
	      {  
	    	  vect[start] = 0; 
	    	  vect[0] = start;  
	      }
	      // Consequently, we start the arrangements at [1], NOT [0].
	          tour(1, vect);
	   }

	   // Used below in generating arrangements.
	   private static void swap ( int[] x, int p, int q )
	   {  int tmp = x[p];  x[p] = x[q]; x[q] = tmp;  }

	   // Recursive generation of the permutatio vectors that constitute
	   // possible paths.
	   private static void tour(int index, int[] vect)
	   {
	      if ( index == n )      // I.e., we have a full arrangement vector
	      {  
	    	  Tour current;

	         if ( matrixWeight[vect[n-1]][vect[0]] > 0 )  // IS there a return edge?
	         {
	        	 //Save the state in the list
	            current = new Tour(vect);
	            if (!tourKnown || current.dist < bestTour)
	            {
	               bestTour = Math.min(current.dist, bestTour);
	               path.add(current);
	               if ( tourDecision )
	                  System.out.println("Accept " + current);
	            }
	            else if (tourDecision)
	            {  
	            	System.out.print ("Too long "); 
	            	partial(vect, n-1);  
	            }
	         }
	         else if (tourDecision)
	         {  
	        	 System.out.print ("No return "); 
	        	 partial ( vect, n-1 );  
	         }
	      }
	      else                   // Continue generating arrangements
	      {
	         int k;         // Loop variable
	         int hold;      // Used in regenerating the original state

	         for ( k = index; k < n; k++ )
	         {
	            swap ( vect, index, k );
	            if ( matrixWeight[vect[index-1]][vect[index]] < 0 )
	               continue;
	            tour ( index+1, vect );
	         }
	         // Restore arrangement
	         hold = vect[index];
	         for ( k = index+1; k < n; k++ )
	            vect[k-1] = vect[k];
	         vect[n-1] = hold;
	      }
	   }
	   
	   static void printTour()
	   {
		   if (tourFound)
		      {  
		    	  System.out.println ("Tours that got better:");
		         for ( Tour optimal : path )
		            System.out.println(optimal);
		      }
		      
		      if ( path.size() == 0 )
		         System.out.println("NO tours discovered.  Exiting.");
		      else
		      {  
		    	 Collections.sort(path);
		      	 
		         System.out.println();
		         System.out.println("Best tour:  ");
		         //Collections.reverseOrder();
		         System.out.println(path.get(0));
		         System.out.println("Due to backtrack and data input the route is in reverse!!! Please read from right to left!!! \n");
		         System.out.println("Worst tour:  ");
		         // First of the pair with this total.
		         System.out.println(path.get(path.size()-1));
		      }
	   }
}

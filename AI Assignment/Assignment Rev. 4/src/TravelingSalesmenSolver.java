import java.util.*;
import java.io.*;


public class TravelingSalesmenSolver
{
	
	//method for getting arguments
	public static String input(String[] args, String arg)
	{
		//loop through arguments
		for(int i=0;i<args.length;i++)
		{
			//change to lower case
			int index = args[i].toLowerCase().indexOf(arg.toLowerCase()+"=");
			
			if(index==0)
			{
				//return the lowercase argument
				return args[i].substring(index+arg.length()+1,args[i].length());
			}
		}
		return null;
	}
   
   public static void main (String[] args)
   {
	    String file = input(args,"file");
		String search = input(args,"search");
	   
      
	   if(file!=null)
	   {
		   
		   try
		   {
			   
			   if(search!=null)
			   {
	   
				if (search.equals("backtrack"))
	   			{
		    		System.out.println("*** running backtrack algorithm ***");
				//run the backtrack search [conventional search]
		    		
		    		//initialise backtrack search
		    		Backtrack bt = new Backtrack();
		    		
		    		//get file input
					String filename = file;
 
					Scanner inp = new Scanner (new java.io.File(filename));

					System.out.println("The data was taken from " + filename);
					
					//create matrix
					bt.init(inp);
					
					//generate paths
					bt.tourPaths();
					
					//print results
					bt.printTour();
	   			}
	   
	   			else if(search.equals("geneticA"))
	   			{
					System.out.println("*** running genetic A algorithm algorithm ***");
	   			//run genetic program that I made
	   				
					//initialise salesman
		    		TravelingSalesman salesman = new TravelingSalesman(8);
			
		    		//print the matrix
					salesman.printCosts();
					
					//initialise the environment for the population
					Enviroment environment = new Enviroment(salesman);

					//run the search
					environment.run();
	   			}
				
	   			else if(search.equals("geneticB"))	
				{
					System.out.println("*** running genetic B algorithm algorithm ***");
				//run the modified genetic railsearch
					
					//initialise the GA
					GA ga = new GA();
					
					//run the GA search
					ga.run();
				}
				
			   }
			   else
	   			{
				   //print error if inputed search was incorrect
		   			System.out.println("Error not a valid algorithm");
		   			System.out.println("Please enter.... search=backtrack || search=geneticA || search=geneticB");
		   			return;
	   			}
			   
	
	      }
		   catch(FileNotFoundException fnf)
		   {
			   //throw file not found exception
			    System.out.println("Error:   File Not Found");
		   }
	   	}
	   	   else
	   	   {
	   		   //if can't find file
				System.out.println("Error:   File not found");			
	   	   }
	   
   }
   
}
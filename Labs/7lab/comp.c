#include <stdio.h>
#include <stdlib.h>

#include "nodes.h"

extern int ch;

extern lex(FILE *);
extern initIdents();
extern printIdents();

extern NODE * program(FILE *);

extern printtree(NODE *,int);

main(int argc,char ** argv)
{  FILE * fin;
   NODE * tree;

   if((fin=fopen(argv[1],"r"))==NULL)
   {  printf("can't open %s\n",argv[1]);
      exit(0);
   }

   initIdents();

   ch = getc(fin);
   lex(fin);
   tree = program(fin);
   fclose(fin);
   printtree(tree,0);
   putchar('\n');
}

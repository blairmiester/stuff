#include <stdio.h>
#include <stdlib.h>

#include "tokens.h"
#include "nodes.h"

extern char * showSymb(int);

spaces(int n)
{  while(n>0)
   {  putchar(' ');
      n--;
   }
}

printseq(NODE * t,int d)
{  if(t->f.b.n1!=NULL)
   {  printtree(t->f.b.n1,d);
      printf(";\n");
   }
   printtree(t->f.b.n2,d);
}

printvar(NODE * t,int d)
{  spaces(d);
   printf("VAR %s",t->f.b.n1->f.id);
}

printassign(NODE * t,int d)
{  spaces(d);
   printf("%s <- ",t->f.b.n1->f.id);
   printtree(t->f.b.n2);
}

printif(NODE * t,int d)
{  spaces(d);
   printf("IF ");
   printtree(t->f.b.n1);
   printf(" THEN\n");
   if(t->f.b.n2->tag==ELSE)
   {  printtree(t->f.b.n2->f.b.n1,d);
      putchar('\n');
      spaces(d);
      printf("ELSE\n");
       printtree(t->f.b.n2->f.b.n2,d);
   }
   else
    printtree(t->f.b.n2,d);
}

printwhile(NODE * t,int d)
{  spaces(d);
   printf("WHILE ");
   printtree(t->f.b.n1);
   printf(" DO\n");
   spaces(d);
   printtree(t->f.b.n2,d);
}

printrepeat(NODE * t,int d)
{  spaces(d);
   printf("REPEAT\n");
   spaces(d);
   printtree(t->f.b.n1,d);
   putchar('\n');
   printf("UNTIL ");
   printtree(t->f.b.n2);
}

printblock(NODE * t,int d)
{  spaces(d);
   printf("BEGIN\n");
   printtree(t->f.b.n1,d+5);
   putchar('\n');
   spaces(d);
   printf("END");
}

printinput(NODE * t,int d)
{  spaces(d);
   printf("INPUT ");
   printtree(t->f.b.n1);
}

printprint(NODE * t,int d)
{  spaces(d);
   printf("PRINT ");
   printtree(t->f.b.n1,d);
}

printtree(NODE * t,int d)
{  switch(t->tag)
   {  case SEMI: printseq(t,d);
                 return;
      case VAR: printvar(t,d);
                return;
      case LARROW: printassign(t,d);
                   return; 
      case IF: printif(t,d);
               return;
      case WHILE: printwhile(t,d);
                  return;
      case REPEAT: printrepeat(t,d);
				   return;
      case BEGIN: printblock(t,d);
                  return;
      case INPUT: printinput(t,d);
                  return;
      case PRINT: printprint(t,d);
                  return;
      case ID: printf("%s",t->f.id);
               return;
      case INT: printf("%d",t->f.value);
                return;
      case MINUS: if(t->f.b.n2==NULL)
                  {  putchar('-');
                     printbra(t->f.b.n1,0);
                     return;
                  }
      case PLUS: case STAR: case SLASH: case LT:
      case LTE: case EQ: case NEQ: case GTE:
      case GT:
               printbra(t->f.b.n1);
               printf("%s",showSymb(t->tag));
               printbra(t->f.b.n2);
               return;
      default: printf("unknown node: %s\n",showSymb(t->tag));
               exit(0);
   }
}

printbra(NODE * e)
{  switch(e->tag)
   {  case ID:
      case INT: printtree(e,0);
                return;
      default: putchar('(');
               printtree(e,0);
               putchar(')');
   }
}

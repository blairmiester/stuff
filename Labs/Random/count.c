#include <stdio.h>
#include <stdlib.h>

int main()

{
	FILE *fp;
	int c, nc, nlines, words;
	char filename[40];
	words = 0;
	nlines = 0;
	nc = 0;


	printf("Enter file name: ");
	gets( filename );
	fp = fopen( filename, "r" );

	if ( fp == NULL )
		{
			printf("Cannot open %s for reading\n", filename );
			exit(1);
		}

	c = getc( fp );

	while ( c != EOF )
		{
			if ( c== '\n' )
			nlines++;

			if ( c== ' ')
			words++;
			nc++;
			c = getc ( fp );
		}

	fclose( fp );

	if ( nc != 0)
		{
			printf("There are %d words in %s\n", words, filename );
			printf("There are %d characters in %s\n", nc, filename );
			printf("There are %d lines in %s\n", nlines, filename );
		}

	else

		printf("File %s is empty\n", filename );
		getchar();
		return 0;

}
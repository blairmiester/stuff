#include <stdio.h>

int IntegerAdd(int i, int j)
{
	int result = i + j;
	return result;
}

int main()
{
	int sum = IntegerAdd(5, 6);
	printf("The addition of 5 and 6 is %d.\n", sum);	
	return 0;
}
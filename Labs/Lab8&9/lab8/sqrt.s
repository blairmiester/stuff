		AREA    ASMain, CODE
; Main

__main	PROC
		EXPORT  __main           
; Assembly Code Below Here

; Init-start
X		RN		1
S		RN		2
D		RN		3
Xinit	EQU		27
		MOV		X,#Xinit
		LSR		S,X,#0x01
; Init-end

; loop-begin

LOOP	CMP		S,#0
		BLE		ENDL ;Go to
		MUL		D,S,S

; iF-start
		CMP		D,X
		ITE		GT		
		SUBGT	S,#1
		BLE		ENDL ;Go to
; iF-end

		B		LOOP ;Go to

; loop-end
; Assembly Code Above Here
ENDL	B ENDL
		ENDP
		END

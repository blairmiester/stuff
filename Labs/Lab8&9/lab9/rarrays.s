		AREA ARRAY, DATA, READWRITE

SRAM	EQU		0x20000200
		MAP		SRAM
MAX		EQU		100
SHIFT	EQU		4 ; Sizeof(int)
A		FIELD	MAX*SHIFT ; Array position 100*sizeof(int)
B		FIELD	MAX*SHIFT ; Array position 100*sizeof(int)
; A is 0x20000200 to 0x20000390
; B is 0x20000390 to 0x20000520

		AREA V, CODE
; Main
__main	PROC
		EXPORT	__main           

a		RN	R1
b		RN	R2
i		RN	R3
t		RN	R4
		LDR a,=A ; Address of A from i = 0
		LDR b,=B ; Address of B from i = 0
		MOV	i,#0 ; Position in array (i = 0)

INT		CMP i,#MAX
		BEQ SET
		STR	i,[a] ; Store value of i in array
		ADD i,#1
		ADD a,#SHIFT
		B 	INT

SET		CMP i,#0
		BEQ ENDL
		SUB a,#SHIFT
		LDR t,[a]
		STR	t,[b]
		SUB i,#1
		ADD	b,#SHIFT
		B SET

ENDL	B 	ENDL
		ENDP
		END
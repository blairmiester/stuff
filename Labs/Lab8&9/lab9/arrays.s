		AREA ARRAY, DATA, READWRITE

SRAM	EQU		0x20000200
		MAP		SRAM
MAX		EQU		100
SHIFT	EQU		4 ; Sizeof(int)
A		FIELD	MAX*SHIFT ; Array of size 100*sizeof(int)
; A is 0x20000000 to 0x20000190

		AREA V, CODE
; Main

__main	PROC
		EXPORT	__main           

array	RN	R1
i		RN	R2
t		RN	R3
		LDR array,=A ; Address of A from i = 0
		MOV	i,#0 ; Position in array (i = 0)

INT		CMP i,#MAX
		BEQ RESET
		STR	i,[array] ; Store value of i in array
		PUSH {i}
		ADD i,#1
		ADD array,#SHIFT
		B 	INT

RESET	CMP i,#0
		BEQ	SET
		SUB array,#SHIFT
		SUB i,#1
		B RESET

SET		CMP i,#MAX
		BEQ ENDL
		POP {t}
		STR	t,[array]
		ADD i,#1
		ADD array,#SHIFT
		B SET

ENDL	B 	ENDL
		ENDP
		END

; ORIGINAL
;FOR		CMP i,#0
;		BEQ	ENDL
;		LDR t,[array]
;		LDR	sec,[spare]
;		STR	sec,[array]
;		STR	t,[spare]
;		ADD spare,#0x04
;		SUB	array,#0x04
;		SUB i,#1
;		B 	FOR
#include <stdio.h>
#include <stdlib.h>
#define MAX 100

int main(){
	int a[MAX];
	int i;
	int t;
	puts("one:\n");
	for(i=0;i<MAX;i++){
		a[i] = i;
		printf("%d ",a[i]);
	}
	puts("\ntwo:\n");
	for(i=0;i<MAX;i++){
		t = a[MAX-i-1];
		a[MAX-i-1] = a[i];
		a[i] = t;
		printf("%d ",a[i]);
	}
	puts("\n");
	exit(EXIT_SUCCESS);
}

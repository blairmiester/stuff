#include <stdio.h>
#include <stdlib.h>

#include "tokens.h"

int ch;
int symb;
int value;
char * id;

struct ident{int symb; char * text; struct ident * next;};

struct ident * idents;

int slength(char * s)
{  int i;
   i =0;
   while(s[i]!='\0')
    i = i+1;
   return i;
}

char * scopy(char * s)
{  char * c;
   int i,l;
   l = slength(s);
   c = (char *)malloc(l);
   i = 0;
   while(i<l)
   {  c[i] = s[i];
      i = i+1;
   }
   return c;
}

char * showSymb(int symb)
{  switch(symb)
   {  case  BEGIN: return "BEGIN";
      case  DO: return "DO";
      case  ELSE: return "ELSE";
      case  END: return "END";
      case  ID: return "ID";
      case  IF: return "IF";
      case  INT: return "INT";
      case  INPUT: return "INPUT";
      case  PRINT: return "PRINT";
      case  THEN: return "THEN";
      case  VAR: return "VAR";
      case  WHILE: return "WHILE";
      case	REPEAT: return "REPEAT";
      case	UNTIL: return "UNTIL";
      case  PLUS: return "+";
      case  MINUS: return "-";
      case  STAR: return "*";
      case  SLASH: return "/";
      case  LBRA: return "(";
      case  RBRA: return ")";
      case  LT: return "<";
      case  LTE: return "<=";
      case  EQ: return "=";
      case  NEQ: return "!=";
      case  GTE: return ">=";
      case  GT: return ">";
      case  LARROW: return "<-";
      case SEMI: return ";";
      case EOF: return "EOF";
      default: printf("bad symbol: %d",symb);
   }
}

printSymb()
{  char * s;
   printf("%s ",showSymb(symb));
   if(symb==ID)
    printf("%s\n",id);
   else
   if(symb==INT)
    printf("%d\n",value);
   else
   printf("\n");
}
   

printIdents()
{  struct ident * ip;
   ip = idents;
   while(ip!=NULL)
   {  printf("%s",showSymb(ip->symb),ip->text);
      if(ip->symb==ID)
       printf(" %s",ip->text);
      printf("\n");
      ip = ip->next;
   }
   printf("\n");
}

addIdent(int s,char * t)
{  struct ident * ip;
   if(idents==NULL)
   {  idents = (struct ident *)malloc(sizeof(struct ident));
      idents->symb = s;
      idents->text = scopy(t);
      idents->next = NULL;
      symb = s;
      id = idents->text;
      return;
   }
   ip = idents;
   while(1)
   {  if(strcmp(ip->text,t)==0)
      {  symb = ip->symb;
         id = ip->text;
         return;
      }
     if(ip->next==NULL)
      break;
     ip = ip->next;
   }
   ip->next = (struct ident *)malloc(sizeof(struct ident));
   ip->next->symb = s;
   ip->next->text = scopy(t);
   ip->next->next = NULL;
   symb = s;
   id = ip->next->text;
   return;
}

initIdents()
{  idents = NULL;
   addIdent(BEGIN,"BEGIN");
   addIdent(DO,"DO");
   addIdent(ELSE,"ELSE");
   addIdent(END,"END");
   addIdent(IF,"IF");
   addIdent(INPUT,"INPUT");
   addIdent(PRINT,"PRINT");
   addIdent(THEN,"THEN");
   addIdent(VAR,"VAR");
   addIdent(WHILE,"WHILE");
   addIdent(REPEAT,"REPEAT");
   addIdent(UNTIL,"UNTIL");
}

getInteger(FILE * fin)
{  symb = INT;
   value=0;
   while(ch>='0' && ch<='9')
   {  value = 10*value+ch-'0';
      ch = getc(fin);
   }
}

getIdent(FILE * fin)
{  char letters[MAXIDENT];
   int i;
   i = 0;
   while(((ch>='a' && ch<='z') || (ch>='A' && ch<='Z') || 
          (ch>='0' && ch<='9')) && i<MAXIDENT-1)
   {  letters[i] = ch;
      ch = getc(fin);
      i++;
   }
   letters[i] = '\0';
   if(i==MAXIDENT-1)
    while((ch>='a' && ch<='z') || (ch>='A' && ch<='Z') ||
          (ch>='0' && ch<='9'))
     ch = getc(fin);
   addIdent(ID,letters);
}
   

lex(FILE * fin)
{  while(ch==' ' || ch=='\n' || ch=='\t')
    ch = getc(fin);
   switch(ch)
   {  case ';': symb = SEMI; break;
      case '+': symb = PLUS; break;
      case '-': symb = MINUS; break;
      case '*': symb = STAR; break;
      case '/': symb = SLASH; break;
      case '(': symb = LBRA; break;
      case ')': symb = RBRA; break;
      case '=': symb = EQ; break;
      case '!': ch = getc(fin);
                if(ch=='='){  symb = NEQ; break; }
                printf("lexical error: = expected after !\n"); exit(0);
      case '<': ch = getc(fin);
                if(ch=='-'){  symb = LARROW; break;  }
                if(ch=='='){  symb = LTE; break;  }
                symb = LT; return;
      case '>': ch = getc(fin);
                if(ch=='=')
                {  symb = GTE; break; }
                symb = GT; return;
      case EOF: symb = EOF; return;
      default: if(ch>='0' && ch<='9')
               {  getInteger(fin); return; }
               if((ch>='a' && ch<='z') || (ch>='A' && ch<='Z'))
               {  getIdent(fin); return;  }
               printf("lexical error: unknown character %c %x\n",ch,ch);
               exit(0);
   }
   ch = getc(fin);
}

#include <stdio.h>
#include <stdlib.h>

#define LM 60

int main(int argc, char ** argv) 
{

   FILE * fin;
   char a[LM];
   int i;
   int lineNo = 0;

   if(argc!=2){
       printf("append: wrong number of arguments\n");
       exit(0);
   }

   fin = fopen(argv[1],"r");
   if(fin==NULL)
   {
       printf("append: can't open %s for reading\n",argv[1]);
       exit(0);
   }

   while ((getLine(fin, a, LM)) != EOF)
   {
        printf("%d ", lineNo);
        for (i = 0; a[i]!= '\n'; i++)
            printf("%d", a[i]);
   }
   fclose(fin);
}

int getLine(FILE * fin,char a[],int n) 
{

    int c, i;

    for(i=0; i<n-1 || (c=getc(fin))!=EOF || c!='\n'; i++)
        a[i] = c;

    if(i=n-1)
    {
        printf("getLine: more than n characters on the line");
        while ((c=getc(fin))!='\n');
        a[i] = c;
        i++;
    }
    
    if (c == EOF)
        return EOF;
    
    else
        return i;
}

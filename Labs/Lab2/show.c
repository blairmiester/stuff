#include <stdio.h>
#include <stdlib.h>

int lineNo = 0;

int getLine(FILE * fin, char a[], int n, char *targ)
{
	lineNo++;
	char ch;
	ch = getc(fin);
	int count = 0;
 	
	while(ch!=EOF && ch!='\n')
	{
		if(count==n)
		{
			printf("Warning! Line too long \n");
			while(ch!='\n')
			{
				ch = getc(fin);
			}
			break;
		}
		a[count] = ch;
		ch = getc(fin);
		count = count+1;
	}
	if(ch=='\n'&& contains(targ,5,a,count)==1)
	{	
		int i;
		printf("%d ",lineNo);
		
		for(i=0;i<count;i++)
		{
		  printf("%c",a[i]);			
		}
		printf("\n");
	}
	if(ch==EOF)
	{
		printf("EOF \n");
		return EOF;
	}
	return count;
}

int contains(char target[], int m, char source[], int n)
{
	int i = 0, s = 0, nC = 0, match = 0;
	while(i<=(n-m) || match!=0)
	{
		if(target[s] == source[nC])
		{
			match++;
			s++;
		}
		else
		{
			match = 0;
			s = 0;
		}
		nC++;
		if(match==m)
		{
			return 1;
		}
		i++;
	}		
	return 0;
}
main(int argc,char ** argv)
{  
	if(argc!=3)
	{  printf("length: wrong number of arguments\n");
      exit(0);
	}
	FILE * fin = fopen(argv[2],"r");
	int n = 50;
	char a[n];
   char *targ = argv[1];

	int returned = getLine(fin,a,n,targ);
	while(returned!=EOF)
	{
		returned = getLine(fin,a,n,targ);
	}
	fclose(fin);
}


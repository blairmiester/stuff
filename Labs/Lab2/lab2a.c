#include <stdio.h>
#include <stdlib.h>

#define LM 60


int main(int argc, char ** argv)
{
  FILE * fin;
  char a[LM];
  int i, lineNo = 0, found;
  
  if(argc != 2)
  {
    printf("wrong number of arguments \n");
      exit(0);
  }
  

  fin = fopen(argv[1], "r");

  if (fin == NULL)
  {
    printf("can't open %s for reading \n", argv[1]);
    exit(0);
  }

  while ((getLine(fin,a,LM))!=EOF)
  {
    found = contains("linux", 5, a, LM);
    lineNo++;
    if(found) printf("%d %s\n",lineNo, a);
  }
   
  fclose(fin);


}


int getLine(FILE * fin, char a[], int n)
{
  int c, i;

    for(i=0; i<n-1 && (c=getc(fin))!=EOF && c!='\n'; i++)
        a[i] = c;
        a[i] = '\0';

    if(i==n-1)
    {
        printf("getLine: more than n characters on the line\n");
        while ((c=getc(fin))!='\n');
        a[i] = c;
        i++;
    }
    
    if (c == EOF)
        return EOF;
    
    else
        return i;
}


int contains(char target[],int m,char source[],int n) 
{
    int i, j, found;

    found = 0;

    for(i=0; i<n-1; i++)
    {
        if(source[i] == target[0])
        {
            i++;

            for(j=1; j<m-1 && (source[i] == target[j]); j++)
                i++;

            if (j=m-1)
                found = 1;
            
        }
    }
    return found;
}

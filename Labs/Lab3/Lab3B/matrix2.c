/* matrix2.c - array of arrays */

#include <stdio.h>
#include <stdlib.h>

int * makeVector(int n)
{  return (int *)malloc(n*sizeof(int));  }

int ** makeMatrix(int m,int n)
{  int ** newm = (int **)malloc(m*sizeof(int *));
   int i;
   for(i=0;i<m;i++)
    newm[i] = makeVector(n);
   return newm;
}

readMatrix(FILE * fin,int * * M,int m,int n)
{  int i,j;

   for(i=0;i<m;i++)
    for(j=0;j<n;j++)
     fscanf(fin,"%d",&(M[i][j]));
}

writeMatrix(FILE * fout,int * * M,int m,int n)
{  int i,j;

   for(i=0;i<m;i++)
   {  for(j=0;j<n;j++)
       fprintf(fout,"%d ",M[i][j]);
      putc('\n',fout);
   }
}

matrixProd(int * * M1,int * * M2,int * * M3,int m,int n)
{  int i,j,k;
   
   for(i=0;i<m;i++)
    for(j=0;j<m;j++)
    {  M3[i][j]=0;
       for(k=0;k<n;k++)
        M3[i][j] = M3[i][j]+M1[i][k]*M2[k][j];
    }
}

main(int argc,char ** argv)
{  FILE * fin;
   int ** m1;
   int ** m2;
   int ** m3;
   int m,n;

   fin = fopen(argv[1],"r");
   fscanf(fin,"%d %d",&m,&n);
   m1 = makeMatrix(m,n);
   readMatrix(fin,m1,m,n);
   m2 = makeMatrix(n,m);
   readMatrix(fin,m2,n,m);
   fclose(fin);
   writeMatrix(stdout,m1,m,n);
   putchar('\n');
   writeMatrix(stdout,m2,n,m);
   putchar('\n');
   m3 = makeMatrix(m,m);
   matrixProd(m1,m2,m3,m,n);
   writeMatrix(stdout,m3,m,m);
}

#include <stdio.h>
#include <stdlib.h>

int max(int a[], int n);
void writeMax(char * s, int a[], int n);

/*Laboratory 3*/
int main(int argc, char** argv) 
{

    FILE * fin;
    int ** M;
    int m, n; /* the number of rows and columns in the matrix */

    if(argc!=2){
        printf("matrix: wrong number of arguments. Please provide a filename\n");
        exit(0);
    }

    fin = fopen(argv[1],"r");
    if(fin==NULL){
        printf("matrix: can't open %s for reading\n",argv[1]);
        exit(0);
    }

    fscanf(fin,"%d %d",&m,&n);

    printf("%d %d\n", m, n);
  
    M = makeMatrix(m, n);

    readMatrix(fin,M,m,n);
}

int max(int a[], int n) 
{

    int i = 1; /* index for array elements being examined. */
    int iMax = 0;/* the index of the largest value in the array a[]. 0 so first
                  *  element is always max, initially. */


    while(i!=n-1) 
    {
        if (a[i] > a[iMax])
            iMax = i;
        i++;
    }
    return iMax;
}

void writeMax(char * s, int a[], int n) 
{

    int i = 0;

    while(i!=n-1) 
    {
        printf("%d ", a[i]);
        i++;
    }
    printf(" - max = %s = %d\n", s, max(a, n)); /* calls on max as it is needed*/

}

int * makeVector(int n)
{  
  return (int *)malloc(n*sizeof(int));  
}

int ** makeMatrix(int m,int n)
{  
   int ** newm = (int **)malloc(m*sizeof(int *));
   int i;
   for(i=0;i<m;i++)
   {
    newm[i] = makeVector(n);
   }
   return newm;
}

readMatrix(FILE * fin,int * * M,int m,int n)
{  
  int i,j;

   for(i=0;i<m;i++)
   {
    for(j=0;j<n;j++)
    {
     fscanf(fin,"%d",&(M[i][j]));
    }
   }
}

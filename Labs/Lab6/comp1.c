#include <stdio.h>
#include <stdlib.h>

extern int ch;

extern lex(FILE *);
extern initIdents();
extern printIdents();

extern program(FILE *,int);

main(int argc,char ** argv)
{  FILE * fin;

   if((fin=fopen(argv[1],"r"))==NULL)
   {  printf("can't open %s\n",argv[1]);
      exit(0);
   }

   initIdents();

   ch = getc(fin);
   lex(fin);
   program(fin,0);
   fclose(fin);
}

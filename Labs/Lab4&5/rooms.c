#include <stdio.h>

#define MAX 10
struct room * rooms[MAX];
int rp;

struct door 
{
  char * name; struct room * room;
};

struct room 
{
  char * name; struct door * doors[4]; int dp;
};

struct room * r; //current room

struct door * newDoor(char * name)
{
  struct door d = { name, NULL }; //create door
  struct door * dp = &d; //point to the address of door
  return dp; //return that pointer
}

struct room * newRoom(char * name)
{
  struct room r = { name, NULL, 0 };
  struct room * rp = &r;
  return rp;
}

void showRoom(struct room * r){
  int i; 
  //struct room 
  printf("Current: %s\n", (*r).name);

  for(i=0;i<(r->dp);i++)
  {
    printf("%d %s", i+1, r->doors[i]); //alt. way of writing memeber of derefenced pointer
  }
}

char * readLine(FILE * fin)
{
  int c, i;
  char * line;
  while((c=getc(fin)!='\n' && c!=EOF))
  {
    line[i] = c;
    i++;
  }
  if (c == '\n')
  {
    return line;
  }
  else if(c==EOF)
  {
    return NULL;
  }
}

void readRooms(FILE * fin)
{
  int i=0;
  char * line = readLine(fin);
  while(line!=NULL)
  {
      rooms[rp] = newRoom(line);
      i=0;
      while(line!="*" && i<4)
      {
        line = readLine(fin);
        (*rooms[rp]).doors[i] = newDoor(line); //Create a door for current room
        connect(line); //and connect that door's pointer.
        (*rooms[rp]).dp++;
        i++;
      }
      rp++;
  }
}

void showRooms()
{
  int i=0;
  for(i=0; i<rp; i++)
  printf("%s\n", rooms[i]);
}

void connect(char * cdoor)
{
  int i=0;
  for(i=0; i<rp; i++)
  if(rooms[i] == cdoor
}

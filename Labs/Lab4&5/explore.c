#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
  * Room 
  * name - string
  * doors - array door
  * doorCount - int
**/
struct room {char * name; struct door * doors[4]; int dp;};

/**
  * Door
  * Stores name of connecting room
  * Stores pointer to adjoining room
**/
struct door {char * name; struct room * aroom;};


/**
  * Some globals and constants
**/
#define MAX_ROOMS 10
int rp=0;
int rc=0;
struct room * rooms[MAX_ROOMS];

struct room * newRoom(char * name) 
{
    struct room * aRoom;
    aRoom = (struct room *) malloc(sizeof(struct room));
    aRoom->name = name;
    return aRoom;
}

struct door * newDoor(char * name) 
{
    struct door * aDoor;
    aDoor = (struct door *) malloc(sizeof(struct door));
    aDoor->name = name;
    return aDoor;
}

/**
  * Displays the room
**/
void showRoom(struct room * myRoom) 
{
    int i=0;

    printf("%s \n", myRoom->name);

    //Loop through doors in struct
    for (i=0;i < myRoom->dp; i++) 
    {
        printf("%d. %s\n", i+1, myRoom->doors[i]->name);
    }
}

char * readLine(FILE * fin) 
{
    char ch;
    int maxLine = 20;
    char * out;
    out = (char *) malloc(sizeof(char[20]));
    int i=0;

    ch = getc(fin);
    while (ch != '\n') 
    {
        if (ch == EOF) 
        {
            out = NULL;
            return out;
        }
        
        if (i < maxLine - 1) 
        {
            out[i] = ch;
            i++;
        } else 
        {
            break;
        }

        ch = getc(fin);
    }
    out[i+1] = '\0';

    return out;
}

/**
  * Read rooms from file and add
  * to stack
**/
void readRooms(FILE * fin) 
{
    int i=0;
    int doorc;

    char * line;

    while (i<MAX_ROOMS) 
    {
        
        line = readLine(fin);
        if (line == NULL) 
        {
            //End of line exit function
            return;
        }
        rooms[i] = newRoom(line);
        doorc=0;

        //Add to room count
        rc++;
        
        line = readLine(fin);

        //Go through the connected rooms
        while (line[0] != '*') 
        {
            if (line == NULL) 
            {
               return;
            }

            //Create new door
            rooms[i]->doors[doorc] = newDoor(line);
            rooms[i]->dp = rooms[i]->dp + 1;
            doorc++;
            //Just hoping there aren't more than 4 doors in file lol :)
            line = readLine(fin);
        }
        i++;
    }
}

/**
  * Go through room stack and print out 
  * all the rooms and their doors.
**/
void showRooms() 
{
    int i;
    int j;
    
    struct room * curRoom;

    for (i=0; i<rc; i++) 
    {
        curRoom = rooms[i];    
        printf("%s\n", curRoom->name);

        //Go through connections
        for (j=0; j<curRoom->dp; j++) 
        {
            printf("  %s\n", curRoom->doors[j]->name);
        }
    }
}
/**
  * Go through rooms and set 
  * the pointer for doors to
  * the adjoining room
**/
void connect() 
{

    int i;
    int j;
    int z;

    struct room * curRoom;
    struct room * sRoom;
    struct door * aDoor;

    for (i=0; i<rc; i++)
    {
        curRoom = rooms[i];
        //Go through connections
        for (j=0; j<curRoom->dp; j++)
        {
            aDoor = curRoom->doors[j];
            for (z=0; z<rc; z++)
            {
                sRoom = rooms[z];
                if (strcmp(aDoor->name, sRoom->name) == 0)
                {
                    aDoor->aroom = sRoom;
                    //If found, stop
                    break;
                }
            }
        }
    }
}

int main(int argc, char ** argv) 
{

    struct room * r;
    FILE * fin;
    int in;
    struct door * d;

    //Ensure argument count is correct
    if (argc != 2) 
    {
        printf("explore: 1 argument expected\n$: explore ROOMFILE");
        exit(1);
    }

    //Open file
    fin =  fopen(argv[1], "r");
    if (fin == NULL) 
    {
        printf("Can't read file: %s", argv[1]);
    }

    readRooms(fin);
    connect();

    //Initial room
    r = rooms[0];

    printf("\House Explorer\n\n\
  * Enter room numbers to move through level\n\
  * Type -1 to quit\n\
  * There is no goal\n");

    //Should really add a goal to the game
    while(1) 
    {
        showRoom(r);
        printf("enter door number>\n");

        scanf("%d", &in);

        //Quit on -1
        if(in==-1)
        {
            break;
        }
        while (in > r->dp || in<1) 
        {
            printf("explore: try again>\n");
            scanf("%d", &in);
        }
        r = r->doors[in-1]->aroom;
    }

    //Close file
    fclose(fin);

    exit(0);
}

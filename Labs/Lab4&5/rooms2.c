#include <stdio.h>
#include <stdlib.h>

#define MAX 10
struct room * rooms[MAX];
int rp;

struct door {char * name; struct room * room;};
struct room {char * name; struct door * doors[4]; int dp;};

struct room * r; //current room

struct door * newDoor(char * name)
{
  struct door d = { name, NULL }; //create door
  struct door * doorp = &d; //point to the address of door
  return doorp; //return that pointer
}

struct room * newRoom(char * name)
{ //room creation similar to door creation
  struct room r = { name, NULL, 0 }; 
  struct room * roomp = &r;
	return roomp;
}

void showRoom(struct room * r)
{
  int i;
  //struct room 
  printf("Current: %s\n", (*r).name);

  for(i=0;i<(r->dp);i++)
    printf("%d %s", i+1, r->doors[i]); //alt. way of writing member of derefenced pointer
}

char * readLine(FILE * fin)
{
  int c, i;
  char * line;
  //free(line);
  line = (char *) malloc(10*sizeof(char));
  c = 0;
  i = 0;
  while((c=getc(fin)!='\n' && c!=EOF))
  {
    line[i] = c;
    i++;
  }
  line[i] = '\0';
  if (c == '\n')
    return line;
  else if(c==EOF)
    return NULL;
}

void readRooms(FILE * fin)
{
  int i=0;
  char * line = readLine(fin);
  while(line!=NULL)
  {	
      struct room * roomc = newRoom(line);
      rooms[rp] = roomc;
      i=0;
      while(line!="*" && i<4)
      {
        line = readLine(fin);
        (*roomc).doors[i] = newDoor(line); //Create a door for current room
        connect(line); //and connect that door's pointer.
        (*rooms[rp]).dp++;
        i++;
      }
      rp++;
  }
}

void showRooms()
{
  int i=0;
  for(i=0; i<rp; i++)
  {
    printf("%s\n", rooms[i]);
  }
}


void connect(char * cdoor)
{
  int i=0;
  for(i=0; i<rp; i++)
    if((*rooms[i]).name == cdoor)
      (*rooms[rp]).doors[i];
}

main(int argc, char ** argv)
{
  FILE * fin;
  if(argc!=2)
   {
       printf("explore: wrong number of arguments\n");
       exit(0);
   }

   fin = fopen(argv[1],"r");
   if(fin==NULL)
   {
       printf("explore: can't open %s for reading\n",argv[1]);
       exit(0);
   }
   readRooms(fin);
   showRoom(r);
}


#include <stdio.h>
#include <stdlib.h>

#include "tokens.h"
#include "nodes.h"

extern int ch;
extern int symb;
extern int value;
extern char * id;

extern lex(FILE *);
extern printSymb();
extern char * showSymb(int);

NODE * newInt(int v)
{  NODE * n;
   n = (NODE *)malloc(sizeof(NODE));
   n->tag = INT;
   n->f.value = v;
   return n;
}

NODE * newId(char * i)
{  NODE * n;
   n = (NODE *)malloc(sizeof(NODE));
   n->tag = ID;
   n->f.id = i;
   return n;
}

NODE * newNode(int tag)
{  NODE * n;
   n = (NODE *)malloc(sizeof(NODE));
   n->tag = tag;
   n->f.b.n1 = NULL;
   n->f.b.n2 = NULL;
   return n;
}

showTree(NODE * tree,int depth)
{  int i;
   if(tree==NULL)
    return;
   for(i=0;i<depth;i++)
    putchar('-');
   switch(tree->tag)
   {  case ID: printf("%s\n",tree->f.id);
               return;
      case INT: printf("%d\n",tree->f.value);
               return;
      default: printf("%s\n",showSymb(tree->tag));
               showTree(tree->f.b.n1,depth+1);
               showTree(tree->f.b.n2,depth+1);
   }
}

error(char * rule,char * message)
{  printf("%s: found %s\n",rule,showSymb(symb));
   printf("%s: %s\n",rule,message);
   exit(0);
}

NODE * program(FILE * fin)
{  extern NODE * declarations(FILE *);
   extern NODE * commands(FILE *);
   NODE * p;
   p = newNode(SEMI);
   if(symb==VAR)
    p->f.b.n1 = declarations(fin);
   p->f.b.n2 = commands(fin);
   return p;
}

NODE * declarations(FILE * fin)
{  NODE * d;
   extern NODE * declaration(FILE *);
   d = declaration(fin);
   if(symb==SEMI)
   {  lex(fin);
      if(symb==VAR)
      {  NODE * d1;
         d1 = d;
         d = newNode(SEMI);
         d->f.b.n1 = d1;
         d->f.b.n2 = declarations(fin);
      }
   }
   else
    error("declarations","; expected");
   return d;
}

NODE * declaration(FILE * fin)
{  NODE * d;
   if(symb!=VAR)
    error("declaration","VAR expected");
   lex(fin);
   if(symb!=ID)
    error("declaration","identifier expected");
   d = newNode(VAR);
   d->f.b.n1 = newId(id);
   lex(fin);
   return d;
}

NODE * commands(FILE * fin)
{  extern NODE * command(FILE *);
   NODE * c;
   c = command(fin);
   if(symb==SEMI)
   {  NODE * c1;
      c1 = c;
      c = newNode(SEMI);
      c->f.b.n1 = c1;
      lex(fin);
      c->f.b.n2 = commands(fin);
   }
   return c;
}

NODE * command(FILE * fin)
{  extern NODE * assign(FILE *);
   extern NODE * ifComm(FILE *);
   extern NODE * input(FILE *);
   extern NODE * print(FILE *);
   extern NODE * whileComm(FILE *);
   extern NODE * repeatComm(FILE *);
   extern NODE * block(FILE *);
   switch(symb)
   {  case ID: return assign(fin);
      case IF: lex(fin);
               return ifComm(fin);

      case WHILE: lex(fin);
                  return whileComm(fin);
      case REPEAT: lex(fin);
                   return repeatComm(fin);
      case BEGIN: lex(fin);
                  return block(fin);
      case INPUT: lex(fin);
                  return input(fin);
      case PRINT: lex(fin);
                  return print(fin);
      default: error("command","BEGIN/IF/INPUT/PRINT/WHILE/REPEAT/identifier expected\n");
   }
}

NODE * assign(FILE * fin)
{  extern NODE * expr(FILE *);
   NODE * a;
   char * i;
   i = id;
   lex(fin);
   if(symb!=LARROW)
    error("assign","<- expected\n");
   a = newNode(LARROW);
   a->f.b.n1 = newId(i);
   lex(fin);
   a->f.b.n2 = expr(fin);
}

NODE * ifComm(FILE * fin)
{  extern NODE * condexp(FILE *);
   NODE * c;
   NODE * t;
   c = newNode(IF);
   c->f.b.n1 = condexp(fin);
   if(symb!=THEN)
    error("if","THEN expected\n");
   lex(fin);
   t = command(fin);
   if(symb==ELSE)
   {  lex(fin);
      c->f.b.n2 = newNode(ELSE);
      c->f.b.n2->f.b.n1 = t;
      c->f.b.n2->f.b.n2 = command(fin);
   }
   else
    c->f.b.n2 = t;
   return c;
}


NODE * whileComm(FILE * fin)
{  extern NODE * condexp(FILE *);
   NODE * w;
   NODE * c;
   c = condexp(fin);
   if(symb!=DO)
    error("while","DO expected\n");
   lex(fin);
   w = newNode(WHILE);
   w->f.b.n1 = c;
   w->f.b.n2 = command(fin);
   return w;
}

NODE * repeatComm(FILE * fin)
{  extern NODE * condexp(FILE *);
   NODE * w;
   NODE * c;
   w = newNode(REPEAT);
   c = command(fin);
   if(symb!=UNTIL)
    error("repeat","UNTIL expected\n");
   lex(fin);
   w->f.b.n1 = c;
   w->f.b.n2 = condexp(fin);
   return w;
}

NODE * block(FILE * fin)
{  NODE * b;
   b = newNode(BEGIN);
   b->f.b.n1 = program(fin);
   if(symb!=END)
    error("block","END expected\n");
   lex(fin);
   return b;
}

NODE * input(FILE * fin)
{  NODE * i;
   i = newNode(INPUT);
   if(symb!=ID)
    error("input","identifier expected\n");
   i->f.b.n1 = newId(id);
   lex(fin);
   return i;
}

NODE * print(FILE * fin)
{  extern NODE * expr(FILE *);
   NODE * p;
   p = newNode(PRINT);
   p->f.b.n1 = expr(fin);
   return p;
}

NODE * condexp(FILE * fin)
{  extern NODE * expr(FILE *);
   NODE * e;
   NODE * c;
   e = expr(fin);
   switch(symb)
   {  case LT:
      case LTE:
      case EQ:
      case NEQ:
      case GTE:
      case GT: c = newNode(symb);
               c->f.b.n1 = e;
               lex(fin);
               c->f.b.n2 = expr(fin);
               return c;
      default: error("condexp","comparison operator expected\n");
   }
}

NODE * expr(FILE * fin)
{  extern NODE * term(FILE *);
   NODE * e;
   NODE * e1;
   e = term(fin);
   while(symb==PLUS || symb==MINUS)
   {  e1 = e;
      e = newNode(symb);
      lex(fin);
      e->f.b.n1 = e1;
      e->f.b.n2 = term(fin);
   }
   return e;
}

NODE * term(FILE * fin)
{  extern NODE * factor(FILE *);
   NODE * t;
   NODE * t1;
   t = factor(fin);
   while(symb==STAR || symb==SLASH)
   {  t1 = t;
      t = newNode(symb);
      lex(fin);
      t->f.b.n1 = t1;
      t->f.b.n2 = term(fin);
   }
   return t;
} 
 
NODE * factor(FILE * fin)
{  extern NODE * base(FILE *);
   NODE * f;
   if(symb==MINUS)
   {  lex(fin);
      f = newNode(MINUS);
      f->f.b.n1 = base(fin);
      return f;
   }
   return base(fin);
}
  
NODE * base(FILE * fin)
{  NODE * b;
   switch(symb)
   {  case ID: b = newId(id);
               break;
      case INT: b = newInt(value);
                break;
      case LBRA: lex(fin);
                 b = expr(fin);
                 if(symb!=RBRA)
                  error("base","( expected\n");
                 break;
      default: error("base","(, identifier or integer expected\n");
   }
   lex(fin);
   return b;
}

#include <stdio.h>
#include <stdlib.h>

#include "tokens.h"
#include "nodes.h"

extern int ch;

extern initIdents();
extern char * showSymb(int);
extern lex(FILE *);

extern NODE * program(FILE *);

spaces(int n)
{  while(n>0)
   {  putchar(' ');
      n--;
   }
}

gencseq(NODE * t,int d)
{  if(t->f.b.n1!=NULL)
    genctree(t->f.b.n1,d);
   genctree(t->f.b.n2,d);
}

gencvar(NODE * t,int d)
{  spaces(d);
   printf("int %s;\n",t->f.b.n1->f.id);
}

gencassign(NODE * t,int d)
{  spaces(d);
   printf("%s = ",t->f.b.n1->f.id);
   gencexp(t->f.b.n2);
   printf(";\n");
}

gencif(NODE * t,int d)
{  spaces(d);
   printf("if(");
   gencexp(t->f.b.n1);
   printf(")\n");
   if(t->f.b.n2->tag==ELSE)
   {  genctree(t->f.b.n2->f.b.n1,d);
      spaces(d);
      printf("else\n");
      genctree(t->f.b.n2->f.b.n2,d);
   }
   else
    genctree(t->f.b.n2,d);
}

gencwhile(NODE * t,int d)
{  spaces(d);
   printf("while(");
   gencexp(t->f.b.n1);
   printf(")\n");
   genctree(t->f.b.n2,d);
}

gencrepeat(NODE * t,int d)
{  spaces(d);
   printf("do\n");
   genctree(t->f.b.n1);
   printf("while(!");
   gencexp(t->f.b.n2);
   printf(");\n");
}

gencblock(NODE * t,int d)
{  spaces(d);
   printf("{\n");
   genctree(t->f.b.n1,d+3);
   spaces(d);
   printf("}\n");
}

gencinput(NODE * t,int d)
{  spaces(d);
   printf("printf(\"INPUT> \");\n");
   printf("scanf(\""); putchar('%'); printf("d\",&");
   gencexp(t->f.b.n1);
   printf(");\n");
}

gencprint(NODE * t,int d)
{  spaces(d);
   printf("printf(\""); putchar('%'); printf("d\\n\",");
   gencexp(t->f.b.n1,d);
   printf(");\n");
}

genctree(NODE * t,int d)
{  switch(t->tag)
   {  case SEMI: gencseq(t,d);
                 return;
      case VAR: gencvar(t,d);
                return;
      case LARROW: gencassign(t,d);
                   return; 
      case IF: gencif(t,d);
               return;
      case WHILE: gencwhile(t,d);
                  return;
      case REPEAT: gencrepeat(t,d);
				   return;
      case BEGIN: gencblock(t,d);
                  return;
      case INPUT: gencinput(t,d);
                  return;
      case PRINT: gencprint(t,d);
                  return;
      default: printf("unknown node: %s\n",showSymb(t->tag));
               exit(0);
   }
}

gencexp(NODE * e)
{  switch(e->tag)
   {  case ID: printf("%s",e->f.id);
               return;
      case INT: printf("%d",e->f.value);
                return;
      case MINUS: if(e->f.b.n2==NULL)
                  {  putchar('-');
                     gencbra(e->f.b.n1);
                     return;
                  }
      case EQ: gencbra(e->f.b.n1);
               printf("==");
               gencbra(e->f.b.n2);
			   return;
      default: gencbra(e->f.b.n1);
               printf("%s",showSymb(e->tag));
               gencbra(e->f.b.n2);
   }
}

gencbra(NODE * e)
{  switch(e->tag)
   {  case ID:
      case INT: gencexp(e);
                return;
      default: putchar('(');
               gencexp(e);
               putchar(')');
   }
}

main(int c,char ** argv)
{  FILE * fin;
   NODE * tree;
   
   if((fin=fopen(argv[1],"r"))==NULL)
   {  printf("can't open %s\n",argv[1]);
      exit(0);
   }
   initIdents();
   ch = getc(fin);
   lex(fin);
   tree = program(fin);
   printf("#include <stdio.h>\n");
   printf("main(int argc,char ** argv)\n");
   printf("{\n");
   genctree(tree,3);
   printf("}\n");
}

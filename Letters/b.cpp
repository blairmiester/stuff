//
//  b.cpp
//  letters
//
//  Created by Blair Murray on 27/09/2012.
//  Copyright (c) 2012 Blair Murray. All rights reserved.
//

//#include "stdafx.h" //Only required for windows
#include <stdlib.h>
//#include <GL/glut.h> //linux
#include <glut/glut.h> //mac
#include <math.h>
#include <stdio.h>
#include "3DCurve.h"
#include "b.h"
#include "cube.h"


static double theta_stop1 = 90;

void drawLetterB()
{
    glPushMatrix();
    glTranslatef(-0.25,0,0);
    glScalef(0.5,4.5,1);
    cube();
	glPopMatrix();
    
    glPushMatrix();
    
    glTranslatef(0, 1, 0);
    glRotatef(0, 0, 0, 1);
    glScalef(0.5, 0.5, 1);
    
    draw3Dcurve  (1.0,          //depth
				  1.5,          //inner radius
				  2.5,          //outer radius
				  0.0,          //start angle
				  theta_stop1,  //stop angle
				  5.0);         //anular increments
    
    
    
    glPopMatrix();
    
    /////////////////////////////
    
    
    glPushMatrix();
    
    glTranslatef(0, 1, 0);
    glRotatef(270, 0, 0, 1);
    glScalef(0.5, 0.5, 1);
    
    draw3Dcurve  (1.0,          //depth
				  1.5,          //inner radius
				  2.5,          //outer radius
				  0.0,          //start angle
				  theta_stop1,  //stop angle
				  5.0);         //anular increments
    
    
    
    glPopMatrix();
    
    /////////////////////////////
    
    glPushMatrix();
    
    glTranslatef(0, -1, 0);
    glRotatef(0, 0, 0, 1);
    glScalef(0.5, 0.5, 1);
    
    draw3Dcurve  (1.0,          //depth
				  1.5,          //inner radius
				  2.5,          //outer radius
				  0.0,          //start angle
				  theta_stop1,  //stop angle
				  5.0);         //anular increments
    
    
    
    glPopMatrix();
    
    /////////////////////////////
    
    glPushMatrix();
    
    glTranslatef(0, -1, 0);
    glRotatef(270, 0, 0, 1);
    glScalef(0.5, 0.5, 1);
    
    draw3Dcurve  (1.0,          //depth
				  1.5,          //inner radius
				  2.5,          //outer radius
				  0.0,          //start angle
				  theta_stop1,  //stop angle
				  5.0);         //anular increments
    
    
    
    glPopMatrix();

    
    
}
//
//  l.cpp
//  letters
//
//  Created by Blair Murray on 27/09/2012.
//  Copyright (c) 2012 Blair Murray. All rights reserved.
//

//#include "stdafx.h" //Only required for windows
#include <stdlib.h>
//#include <GL/glut.h> //linux
#include <glut/glut.h> //mac
#include <math.h>
#include <stdio.h>
#include "3DCurve.h"
#include "l.h"
#include "cube.h"


//static double theta_stop1 = 90;


void drawLetterL()
{
    glPushMatrix();
    glTranslatef(0,0,0);
    glScalef(0.5,4.5,1);
    cube();
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0.5,-2,0);
    glRotatef(90, 0, 0, 1);
    glScalef(0.5,1,1);
    cube();
    glPopMatrix();
}

